package util;

/**
 * @author zrx
 */
public class CastUtils {
	@SuppressWarnings("unchecked")
    public static <T> T cast(Object object) {
        return (T) object;
    }
}
