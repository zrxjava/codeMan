package util;

import constant.ChildWindowConstant;
import constant.CodeConstant;
import constant.MenuLevel;
import entity.DataSourceModel;
import entity.Parameters;
import entity.TablesQueryModel;
import entity.cmnSys.CmSysButton;
import entity.cmnSys.CmSysMenu;

import javax.swing.*;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class DataUtils {


	/**
	 * 已经存在的表
	 */
	private static final List<String> EXIST_TABLES = new ArrayList<>(10);

	/**
	 * @param columnsEng
	 */
	public static String getSqlParam(String columnsEng) {

		String sqlParamColumEng = "";

		if (columnsEng.contains(".")) {
			columnsEng = columnsEng.toLowerCase();
			String[] nameArr = columnsEng.split("\\.");
			sqlParamColumEng = makeSqlParam(nameArr);
		} else if (columnsEng.contains("_")) {
			columnsEng = columnsEng.toLowerCase();
			String[] nameArr = columnsEng.split("_");
			sqlParamColumEng = makeSqlParam(nameArr);
		} else if (columnsEng.contains("-")) {
			columnsEng = columnsEng.toLowerCase();
			String[] nameArr = columnsEng.split("-");
			sqlParamColumEng = makeSqlParam(nameArr);
		} else {
			if (columnsEng.matches("^[A-Z|0-9A-Z]+$")) {
				sqlParamColumEng = columnsEng.toLowerCase();
			} else {
				if (columnsEng.length() > 2) {
					sqlParamColumEng = columnsEng.substring(0, 2).toLowerCase() + columnsEng.substring(2);
				} else {
					sqlParamColumEng = columnsEng.toLowerCase();
				}

			}
		}

		return sqlParamColumEng;
	}

	private static String makeSqlParam(String[] nameArr) {

		StringBuilder sqlParamColumEng = new StringBuilder();
		for (int j = 0; j < nameArr.length; j++) {
			if (j == 0) {
				sqlParamColumEng.append(nameArr[j]);
			} else if (nameArr[j].length() > 1) {
				sqlParamColumEng.append(nameArr[j].substring(0, 1).toUpperCase()).append(nameArr[j].substring(1));
			} else {
				sqlParamColumEng.append(nameArr[j].toUpperCase());
			}
		}
		if (sqlParamColumEng.length() > 2) {
			sqlParamColumEng = new StringBuilder(sqlParamColumEng.substring(0, 2).toLowerCase() + sqlParamColumEng.substring(2));
		} else {
			sqlParamColumEng = new StringBuilder(sqlParamColumEng.toString().toLowerCase());
		}
		return sqlParamColumEng.toString();
	}

	/**
	 * 生成权限管理需要的表并初始化数据
	 *
	 * @param parameters 参数
	 */
	public static void makeAuth(Parameters parameters) {
		//清空表
		EXIST_TABLES.clear();
		Connection connection = null;
		Statement statement = null;
		ResultSet rs = null;
		try {
			if (parameters.isCloudModel()) {
				//如果是cloudModel模式，数据源需要换为系统服务数据源
				Parameters cloudSysDsName = parameters.getCloudSysDsParam();
				connection = DBUtils.getConnection(cloudSysDsName);
			} else {
				connection = DBUtils.getConnection(parameters);
			}
			assert connection != null;
			statement = connection.createStatement();
			if (CodeConstant.MYSQL.equals(parameters.getDataBaseTypeVal())) {
				//cm_sys_user
				createTable("cm_sys_user", statement,
						"select count(*)  from information_schema.tables t where t.table_schema ='" + parameters.getDataBaseNameVal() + "' and t.table_name ='cm_sys_user'",
						"CREATE TABLE `cm_sys_user` (\n" +
								"  `user_id` bigint(20) NOT NULL,\n" +
								"  `role_id` bigint(20) DEFAULT NULL,\n" +
								"  `username` varchar(30) DEFAULT NULL,\n" +
								"  `password` varchar(100) DEFAULT NULL,\n" +
								"  `create_time` datetime DEFAULT CURRENT_TIMESTAMP,\n" +
								"  `update_time` datetime DEFAULT NULL,\n" +
								"  `create_user_id` bigint(20) DEFAULT NULL,\n" +
								"  `update_user_id` bigint(20) DEFAULT NULL,\n" +
								"  PRIMARY KEY (`user_id`)\n" +
								") ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;");
				//cm_sys_menu
				createTable("cm_sys_menu", statement,
						"select count(*)  from information_schema.tables t where t.table_schema ='" + parameters.getDataBaseNameVal() + "' and t.table_name ='cm_sys_menu'",
						"CREATE TABLE `cm_sys_menu` (\n" +
								"  `menu_id` bigint(20) NOT NULL,\n" +
								"  `parent_id` bigint(20) DEFAULT NULL,\n" +
								"  `order_no` bigint(20) DEFAULT NULL,\n" +
								"  `menu_name` varchar(30) DEFAULT NULL,\n" +
								"  `menu_icon` varchar(30) DEFAULT NULL,\n" +
								"  `url_address` varchar(100) DEFAULT NULL,\n" +
								"  `can_del` int(1) DEFAULT NULL,\n" +
								"  `create_time` datetime DEFAULT CURRENT_TIMESTAMP,\n" +
								"  `update_time` datetime DEFAULT NULL,\n" +
								"  `create_user_id` bigint(20) DEFAULT NULL,\n" +
								"  `update_user_id` bigint(20) DEFAULT NULL,\n" +
								"  PRIMARY KEY (`menu_id`)\n" +
								") ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;");
				//cm_sys_role
				createTable("cm_sys_role", statement,
						"select count(*)  from information_schema.tables t where t.table_schema ='" + parameters.getDataBaseNameVal() + "' and t.table_name ='cm_sys_role'",
						"CREATE TABLE `cm_sys_role` (\n" +
								"  `role_id` bigint(20) NOT NULL,\n" +
								"  `role_name` varchar(30) DEFAULT NULL,\n" +
								"  `create_time` datetime DEFAULT CURRENT_TIMESTAMP,\n" +
								"  `update_time` datetime DEFAULT NULL,\n" +
								"  `create_user_id` bigint(20) DEFAULT NULL,\n" +
								"  `update_user_id` bigint(20) DEFAULT NULL,\n" +
								"  PRIMARY KEY (`role_id`) USING BTREE\n" +
								") ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;");
				//cm_sys_button
				createTable("cm_sys_button", statement,
						"select count(*)  from information_schema.tables t where t.table_schema ='" + parameters.getDataBaseNameVal() + "' and t.table_name ='cm_sys_button'",
						"CREATE TABLE `cm_sys_button` (\n" +
								"  `button_id` bigint(20) NOT NULL,\n" +
								"  `menu_id` bigint(20) NOT NULL,\n" +
								"  `button_name` varchar(30) DEFAULT NULL,\n" +
								"  `module_tag_id` varchar(30) DEFAULT NULL,\n" +
								"  `create_time` datetime DEFAULT CURRENT_TIMESTAMP,\n" +
								"  `update_time` datetime DEFAULT NULL,\n" +
								"  `create_user_id` bigint(20) DEFAULT NULL,\n" +
								"  `update_user_id` bigint(20) DEFAULT NULL,\n" +
								"  PRIMARY KEY (`button_id`) USING BTREE\n" +
								") ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;");
				//cm_sys_role_menu
				createTable("cm_sys_role_menu", statement,
						"select count(*)  from information_schema.tables t where t.table_schema ='" + parameters.getDataBaseNameVal() + "' and t.table_name ='cm_sys_role_menu'",
						"CREATE TABLE `cm_sys_role_menu` (\n" +
								"  `role_menu_id` bigint(20) NOT NULL,\n" +
								"  `role_id` bigint(20) DEFAULT NULL,\n" +
								"  `menu_id` bigint(20) DEFAULT NULL,\n" +
								"  `create_time` datetime DEFAULT CURRENT_TIMESTAMP,\n" +
								"  `update_time` datetime DEFAULT NULL,\n" +
								"  `create_user_id` bigint(20) DEFAULT NULL,\n" +
								"  `update_user_id` bigint(20) DEFAULT NULL,\n" +
								"  PRIMARY KEY (`role_menu_id`) USING BTREE\n" +
								") ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;");
				//cm_sys_role_button
				createTable("cm_sys_role_button", statement,
						"select count(*)  from information_schema.tables t where t.table_schema ='" + parameters.getDataBaseNameVal() + "' and t.table_name ='cm_sys_role_button'",
						"CREATE TABLE `cm_sys_role_button` (\n" +
								"  `role_button_id` bigint(20) NOT NULL,\n" +
								"  `role_id` bigint(20) DEFAULT NULL,\n" +
								"  `button_id` bigint(20) DEFAULT NULL,\n" +
								"  `menu_id` bigint(20) DEFAULT NULL,\n" +
								"  `create_time` datetime DEFAULT CURRENT_TIMESTAMP,\n" +
								"  `update_time` datetime DEFAULT NULL,\n" +
								"  `create_user_id` bigint(20) DEFAULT NULL,\n" +
								"  `update_user_id` bigint(20) DEFAULT NULL,\n" +
								"  PRIMARY KEY (`role_button_id`) USING BTREE\n" +
								") ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;");
			} else if (CodeConstant.ORACLE.equals(parameters.getDataBaseTypeVal())) {
				//cm_sys_user
				createTable("cm_sys_user", statement,
						"select count(*) from user_tables where table_name=upper('cm_sys_user')",
						"CREATE TABLE \"CM_SYS_USER\" \n" +
								"   (\t\"USER_ID\" NUMBER(20,0) NOT NULL ENABLE, \n" +
								"\t\"ROLE_ID\" NUMBER(20,0), \n" +
								"\t\"USERNAME\" NVARCHAR2(30), \n" +
								"\t\"PASSWORD\" NVARCHAR2(100), \n" +
								"\t\"CREATE_TIME\" DATE, \n" +
								"\t\"UPDATE_TIME\" DATE, \n" +
								"\t\"CREATE_USER_ID\" NUMBER(20,0), \n" +
								"\t\"UPDATE_USER_ID\" NUMBER(20,0), \n" +
								"\t PRIMARY KEY (\"USER_ID\"))");
				//cm_sys_menu
				createTable("cm_sys_menu", statement,
						"select count(*) from user_tables where table_name=upper('cm_sys_menu')",
						"CREATE TABLE \"CM_SYS_MENU\" \n" +
								"   (\t\"MENU_ID\" NUMBER(20,0) NOT NULL ENABLE, \n" +
								"\t\"PARENT_ID\" NUMBER(20,0), \n" +
								"\t\"ORDER_NO\" NUMBER(20,0), \n" +
								"\t\"MENU_NAME\" NVARCHAR2(30), \n" +
								"\t\"MENU_ICON\" NVARCHAR2(30), \n" +
								"\t\"URL_ADDRESS\" NVARCHAR2(100), \n" +
								"\t\"CAN_DEL\" NUMBER(1,0), \n" +
								"\t\"CREATE_TIME\" DATE, \n" +
								"\t\"UPDATE_TIME\" DATE, \n" +
								"\t\"CREATE_USER_ID\" NUMBER(20,0), \n" +
								"\t\"UPDATE_USER_ID\" NUMBER(20,0), \n" +
								"\t PRIMARY KEY (\"MENU_ID\"))");
				//cm_sys_role
				createTable("cm_sys_role", statement,
						"select count(*) from user_tables where table_name=upper('cm_sys_role')",
						"CREATE TABLE \"CM_SYS_ROLE\" \n" +
								"   (\t\"ROLE_ID\" NUMBER(20,0) NOT NULL ENABLE, \n" +
								"\t\"ROLE_NAME\" NVARCHAR2(30), \n" +
								"\t\"CREATE_TIME\" DATE, \n" +
								"\t\"UPDATE_TIME\" DATE, \n" +
								"\t\"CREATE_USER_ID\" NUMBER(20,0), \n" +
								"\t\"UPDATE_USER_ID\" NUMBER(20,0), \n" +
								"\t PRIMARY KEY (\"ROLE_ID\"))");
				//cm_sys_button
				createTable("cm_sys_button", statement,
						"select count(*) from user_tables where table_name=upper('cm_sys_button')",
						"CREATE TABLE \"CM_SYS_BUTTON\" \n" +
								"   (\t\"BUTTON_ID\" NUMBER(20,0) NOT NULL ENABLE, \n" +
								"\t\"MENU_ID\" NUMBER(20,0) NOT NULL ENABLE, \n" +
								"\t\"BUTTON_NAME\" NVARCHAR2(30), \n" +
								"\t\"MODULE_TAG_ID\" NVARCHAR2(30), \n" +
								"\t\"CREATE_TIME\" DATE, \n" +
								"\t\"UPDATE_TIME\" DATE, \n" +
								"\t\"CREATE_USER_ID\" NUMBER(20,0), \n" +
								"\t\"UPDATE_USER_ID\" NUMBER(20,0), \n" +
								"\t PRIMARY KEY (\"BUTTON_ID\"))");
				//cm_sys_role_menu
				createTable("cm_sys_role_menu", statement,
						"select count(*) from user_tables where table_name=upper('cm_sys_role_menu')",
						"CREATE TABLE \"CM_SYS_ROLE_MENU\" \n" +
								"   (\t\"ROLE_MENU_ID\" NUMBER(20,0) NOT NULL ENABLE, \n" +
								"\t\"ROLE_ID\" NUMBER(20,0), \n" +
								"\t\"MENU_ID\" NUMBER(20,0), \n" +
								"\t\"CREATE_TIME\" DATE, \n" +
								"\t\"UPDATE_TIME\" DATE, \n" +
								"\t\"CREATE_USER_ID\" NUMBER(20,0), \n" +
								"\t\"UPDATE_USER_ID\" NUMBER(20,0), \n" +
								"\t PRIMARY KEY (\"ROLE_MENU_ID\"))");
				//cm_sys_role_button
				createTable("cm_sys_role_button", statement,
						"select count(*) from user_tables where table_name=upper('cm_sys_role_button')",
						"CREATE TABLE \"CM_SYS_ROLE_BUTTON\" \n" +
								"   (\t\"ROLE_BUTTON_ID\" NUMBER(20,0) NOT NULL ENABLE, \n" +
								"\t\"ROLE_ID\" NUMBER(20,0), \n" +
								"\t\"BUTTON_ID\" NUMBER(20,0), \n" +
								"\t\"MENU_ID\" NUMBER(20,0), \n" +
								"\t\"CREATE_TIME\" DATE, \n" +
								"\t\"UPDATE_TIME\" DATE, \n" +
								"\t\"CREATE_USER_ID\" NUMBER(20,0), \n" +
								"\t\"UPDATE_USER_ID\" NUMBER(20,0), \n" +
								"\t PRIMARY KEY (\"ROLE_BUTTON_ID\"))");
			} else if (CodeConstant.POSTGRESQL.equals(parameters.getDataBaseTypeVal())) {
				rs = statement.executeQuery("select count(*) FROM information_schema.schemata WHERE schema_name = 'public';");
				rs.next();
				int count = rs.getInt(1);
				//如果没查到，证明没有这个表，创建public模式
				if (count == 0) {
					statement.executeUpdate("create schema public");
				}
				//cm_sys_user
				createTable("cm_sys_user", statement,
						"select count(*) from information_schema.tables where table_schema='public' AND table_name='cm_sys_user'",
						"CREATE TABLE \"public\".\"cm_sys_user\" (\n" +
								"  \"user_id\" int8 NOT NULL,\n" +
								"  \"role_id\" int8,\n" +
								"  \"username\" varchar(30) COLLATE \"pg_catalog\".\"default\",\n" +
								"  \"password\" varchar(100) COLLATE \"pg_catalog\".\"default\",\n" +
								"  \"create_time\" timestamp(6),\n" +
								"  \"update_time\" timestamp(6),\n" +
								"  \"create_user_id\" int8,\n" +
								"  \"update_user_id\" int8,\n" +
								"  CONSTRAINT \"cm_sys_user_pkey\" PRIMARY KEY (\"user_id\")\n" +
								")");
				//cm_sys_menu
				createTable("cm_sys_menu", statement,
						"select count(*) from information_schema.tables where table_schema='public' AND table_name='cm_sys_menu'",
						"CREATE TABLE \"public\".\"cm_sys_menu\" (\n" +
								"  \"menu_id\" int8 NOT NULL,\n" +
								"  \"parent_id\" int8,\n" +
								"  \"order_no\" int8,\n" +
								"  \"menu_name\" varchar(30) COLLATE \"pg_catalog\".\"default\",\n" +
								"  \"menu_icon\" varchar(30) COLLATE \"pg_catalog\".\"default\",\n" +
								"  \"url_address\" varchar(100) COLLATE \"pg_catalog\".\"default\",\n" +
								"  \"can_del\" int8,\n" +
								"  \"create_time\" timestamp(6),\n" +
								"  \"update_time\" timestamp(6),\n" +
								"  \"create_user_id\" int8,\n" +
								"  \"update_user_id\" int8,\n" +
								"  CONSTRAINT \"cm_sys_menu_pkey\" PRIMARY KEY (\"menu_id\")\n" +
								")");
				//cm_sys_role
				createTable("cm_sys_role", statement,
						"select count(*) from information_schema.tables where table_schema='public' AND table_name='cm_sys_role'",
						"CREATE TABLE \"public\".\"cm_sys_role\" (\n" +
								"  \"role_id\" int8 NOT NULL,\n" +
								"  \"role_name\" varchar(30) COLLATE \"pg_catalog\".\"default\",\n" +
								"  \"create_time\" timestamp(6),\n" +
								"  \"update_time\" timestamp(6),\n" +
								"  \"create_user_id\" int8,\n" +
								"  \"update_user_id\" int8,\n" +
								"  CONSTRAINT \"cm_sys_role_pkey\" PRIMARY KEY (\"role_id\")\n" +
								")");
				//cm_sys_button
				createTable("cm_sys_button", statement,
						"select count(*) from information_schema.tables where table_schema='public' AND table_name='cm_sys_button'",
						"CREATE TABLE \"public\".\"cm_sys_button\" (\n" +
								"  \"button_id\" int8 NOT NULL,\n" +
								"  \"menu_id\" int8 NOT NULL,\n" +
								"  \"button_name\" varchar(30) COLLATE \"pg_catalog\".\"default\",\n" +
								"  \"module_tag_id\" varchar(30) COLLATE \"pg_catalog\".\"default\",\n" +
								"  \"create_time\" timestamp(6),\n" +
								"  \"update_time\" timestamp(6),\n" +
								"  \"create_user_id\" int8,\n" +
								"  \"update_user_id\" int8,\n" +
								"  CONSTRAINT \"cm_sys_button_pkey\" PRIMARY KEY (\"button_id\")\n" +
								")");
				//cm_sys_role_menu
				createTable("cm_sys_role_menu", statement,
						"select count(*) from information_schema.tables where table_schema='public' AND table_name='cm_sys_role_menu'",
						"CREATE TABLE \"public\".\"cm_sys_role_menu\" (\n" +
								"  \"role_menu_id\" int8 NOT NULL,\n" +
								"  \"role_id\" int8,\n" +
								"  \"menu_id\" int8,\n" +
								"  \"create_time\" timestamp(6),\n" +
								"  \"update_time\" timestamp(6),\n" +
								"  \"create_user_id\" int8,\n" +
								"  \"update_user_id\" int8,\n" +
								"  CONSTRAINT \"cm_sys_role_menu_pkey\" PRIMARY KEY (\"role_menu_id\")\n" +
								")");
				//cm_sys_role_button
				createTable("cm_sys_role_button", statement,
						"select count(*) from information_schema.tables where table_schema='public' AND table_name='cm_sys_role_button'",
						"CREATE TABLE \"public\".\"cm_sys_role_button\" (\n" +
								"  \"role_button_id\" int8 NOT NULL,\n" +
								"  \"role_id\" int8,\n" +
								"  \"button_id\" int8,\n" +
								"  \"menu_id\" int8,\n" +
								"  \"create_time\" timestamp(6),\n" +
								"  \"update_time\" timestamp(6),\n" +
								"  \"create_user_id\" int8,\n" +
								"  \"update_user_id\" int8,\n" +
								"  CONSTRAINT \"cm_sys_role_button_pkey\" PRIMARY KEY (\"role_button_id\")\n" +
								")");
			} else if (CodeConstant.SQL_SERVER.equals(parameters.getDataBaseTypeVal())) {
				//cm_sys_user
				createTable("cm_sys_user", statement,
						"select count(*) from sysobjects where name='cm_sys_user'",
						"CREATE TABLE [dbo].[cm_sys_user] (\n" +
								"  [user_id] bigint  NOT NULL,\n" +
								"  [role_id] bigint  NULL,\n" +
								"  [username] nvarchar(30) COLLATE Chinese_PRC_CI_AS  NULL,\n" +
								"  [password] nvarchar(100) COLLATE Chinese_PRC_CI_AS  NULL,\n" +
								"  [create_time] datetime  NULL,\n" +
								"  [update_time] datetime  NULL,\n" +
								"  [create_user_id] bigint  NULL,\n" +
								"  [update_user_id] bigint  NULL,\n" +
								"  CONSTRAINT [PK__cm_sys_u__B9BE370FFE3BA845] PRIMARY KEY CLUSTERED ([user_id]))\n"
				);
				//cm_sys_menu
				createTable("cm_sys_menu", statement,
						"select count(*) from sysobjects where name='cm_sys_menu'",
						"CREATE TABLE [dbo].[cm_sys_menu] (\n" +
								"  [menu_id] bigint  NOT NULL,\n" +
								"  [parent_id] bigint  NULL,\n" +
								"  [order_no] bigint  NULL,\n" +
								"  [menu_name] nvarchar(30) COLLATE Chinese_PRC_CI_AS  NULL,\n" +
								"  [menu_icon] nvarchar(30) COLLATE Chinese_PRC_CI_AS  NULL,\n" +
								"  [url_address] nvarchar(100) COLLATE Chinese_PRC_CI_AS  NULL,\n" +
								"  [can_del] int  NULL,\n" +
								"  [create_time] datetime  NULL,\n" +
								"  [update_time] datetime  NULL,\n" +
								"  [create_user_id] bigint  NULL,\n" +
								"  [update_user_id] bigint  NULL,\n" +
								"  CONSTRAINT [PK__cm_sys_m__4CA0FADCF6ED6F9D] PRIMARY KEY CLUSTERED ([menu_id]))\n"
				);
				//cm_sys_role
				createTable("cm_sys_role", statement,
						"select count(*) from sysobjects where name='cm_sys_role'",
						"CREATE TABLE [dbo].[cm_sys_role] (\n" +
								"  [role_id] bigint  NOT NULL,\n" +
								"  [role_name] nvarchar(30) COLLATE Chinese_PRC_CI_AS  NULL,\n" +
								"  [create_time] datetime  NULL,\n" +
								"  [update_time] datetime  NULL,\n" +
								"  [create_user_id] bigint  NULL,\n" +
								"  [update_user_id] bigint  NULL,\n" +
								"  CONSTRAINT [PK__cm_sys_r__760965CC910C7522] PRIMARY KEY CLUSTERED ([role_id]))\n"
				);
				//cm_sys_button
				createTable("cm_sys_button", statement,
						"select count(*) from sysobjects where name='cm_sys_button'",
						"CREATE TABLE [dbo].[cm_sys_button] (\n" +
								"  [button_id] bigint  NOT NULL,\n" +
								"  [menu_id] bigint  NOT NULL,\n" +
								"  [button_name] nvarchar(30) COLLATE Chinese_PRC_CI_AS  NULL,\n" +
								"  [module_tag_id] nvarchar(30) COLLATE Chinese_PRC_CI_AS  NULL,\n" +
								"  [create_time] datetime  NULL,\n" +
								"  [update_time] datetime  NULL,\n" +
								"  [create_user_id] bigint  NULL,\n" +
								"  [update_user_id] bigint  NULL,\n" +
								"  CONSTRAINT [PK__cm_sys_b__D288541B2D707289] PRIMARY KEY CLUSTERED ([button_id]))\n"
				);
				//cm_sys_role_menu
				createTable("cm_sys_role_menu", statement,
						"select count(*) from sysobjects where name='cm_sys_role_menu'",
						"CREATE TABLE [dbo].[cm_sys_role_menu] (\n" +
								"  [role_menu_id] bigint  NOT NULL,\n" +
								"  [role_id] bigint  NULL,\n" +
								"  [menu_id] bigint  NULL,\n" +
								"  [create_time] datetime  NULL,\n" +
								"  [update_time] datetime  NULL,\n" +
								"  [create_user_id] bigint  NULL,\n" +
								"  [update_user_id] bigint  NULL,\n" +
								"  CONSTRAINT [PK__cm_sys_r__7101AE8A33410FC5] PRIMARY KEY CLUSTERED ([role_menu_id]))\n"
				);
				//cm_sys_role_button
				createTable("cm_sys_role_button", statement,
						"select count(*) from sysobjects where name='cm_sys_role_button'",
						"CREATE TABLE [dbo].[cm_sys_role_button] (\n" +
								"  [role_button_id] bigint  NOT NULL,\n" +
								"  [role_id] bigint  NULL,\n" +
								"  [button_id] bigint  NULL,\n" +
								"  [menu_id] bigint  NULL,\n" +
								"  [create_time] datetime  NULL,\n" +
								"  [update_time] datetime  NULL,\n" +
								"  [create_user_id] bigint  NULL,\n" +
								"  [update_user_id] bigint  NULL,\n" +
								"  CONSTRAINT [PK__cm_sys_r__C9997DED28C833C2] PRIMARY KEY CLUSTERED ([role_button_id]))\n");
			}
			//生成数据
			authDataInit(connection, parameters);
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, "权限管理建表时出错：" + e.getMessage(), "错误",
					JOptionPane.ERROR_MESSAGE);
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException ignored) {
				}
			}
			if (statement != null) {
				try {
					statement.close();
				} catch (SQLException ignored) {
				}
			}
			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException ignored) {
				}
			}
		}
	}

	private static void createTable(String tableName, Statement statement, String countSql, String createSql) {
		try {
			ResultSet rs = statement.executeQuery(countSql);
			rs.next();
			int count = rs.getInt(1);
			//如果没查到，证明没有这个表，创建
			if (count == 0) {
				statement.executeUpdate(createSql);
				//如果有这个表，提示用户需不需要创建
			} else {
				int ifGo = JOptionPane.showConfirmDialog(null,
						"您的数据库中已存在" + tableName + "表" + CodeConstant.NEW_LINE
								+ "是否需要重建表（重建会删除已有表以及表中数据）？",
						"提示", JOptionPane.YES_NO_OPTION);
				if (ifGo == 0) {
					statement.executeUpdate("DROP TABLE " + tableName);
					statement.executeUpdate(createSql);
					//重新建表了，移除
					EXIST_TABLES.remove(tableName);
				} else {
					//没有建表，添加
					EXIST_TABLES.add(tableName);
				}

			}
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, "权限管理建表时出错：" + e.getMessage(), "错误",
					JOptionPane.ERROR_MESSAGE);
		}
	}

	/**
	 * 初始化权限管理表的数据
	 *
	 * @param connection
	 */
	private static void authDataInit(Connection connection, Parameters parameters) {
		PreparedStatement pst;
		try {
			if (!EXIST_TABLES.contains("cm_sys_user")) {
				//初始化cm_sys_user表
				pst = connection.prepareStatement("INSERT INTO cm_sys_user(user_id,role_id,username,password,create_time,create_user_id) VALUES(?,?,?,?,?,?)");
				pst.setLong(1, 1);
				pst.setLong(2, 1);
				pst.setString(3, "admin");
				//前台需要传md5加密后的密码，后台再次加密后比对
				pst.setString(4, Md5Util.digest(Md5Util.digest("123456")));
				pst.setTimestamp(5, new Timestamp(System.currentTimeMillis()));
				pst.setLong(6, 1);
				pst.executeUpdate();
				pst.close();
			}
			if (!EXIST_TABLES.contains("cm_sys_role")) {
				//初始化cm_sys_role表
				pst = connection.prepareStatement("INSERT INTO cm_sys_role(role_id,role_name,create_time,create_user_id) VALUES(?,?,?,?)");
				pst.setLong(1, 1);
				pst.setString(2, "超级管理员");
				pst.setTimestamp(3, new Timestamp(System.currentTimeMillis()));
				pst.setLong(4, 1);
				pst.executeUpdate();
				pst.close();
			}
			if (!EXIST_TABLES.contains("cm_sys_menu")) {
				//初始化cm_sys_menu表
				//创建系统管理，用户管理，菜单管理，角色管理四个基本菜单
				pst = connection.prepareStatement("INSERT INTO cm_sys_menu(menu_id,parent_id,order_no,menu_name,menu_icon,url_address,can_del,create_time,create_user_id) VALUES(?,?,?,?,?,?,?,?,?)");
				pst.setLong(1, 562076320075694080L);
				pst.setLong(2, 0L);
				pst.setLong(3, 562076320075694080L);
				pst.setString(4, "系统管理");
				pst.setString(5, CodeConstant.OLD_THEME.equals(parameters.getThemeVal()) ? "icon icon-chart" : "fa fa fa-bar-chart-o");
				pst.setString(6, "");
				pst.setInt(7, 0);
				pst.setTimestamp(8, new Timestamp(System.currentTimeMillis()));
				pst.setLong(9, 1);
				pst.executeUpdate();
				pst.close();
				pst = connection.prepareStatement("INSERT INTO cm_sys_menu(menu_id,parent_id,order_no,menu_name,menu_icon,url_address,can_del,create_time,create_user_id) VALUES(?,?,?,?,?,?,?,?,?)");
				pst.setLong(1, 562076608706723840L);
				pst.setLong(2, 562076320075694080L);
				pst.setLong(3, 562076608706723840L);
				pst.setString(4, "用户管理");
				pst.setString(5, "");
				pst.setString(6, CodeConstant.OLD_THEME.equals(parameters.getThemeVal()) ? "cm_sys_user/list" : "cmSysUser/list.html");
				pst.setInt(7, 0);
				pst.setTimestamp(8, new Timestamp(System.currentTimeMillis()));
				pst.setLong(9, 1);
				pst.executeUpdate();
				pst.close();
				pst = connection.prepareStatement("INSERT INTO cm_sys_menu(menu_id,parent_id,order_no,menu_name,menu_icon,url_address,can_del,create_time,create_user_id) VALUES(?,?,?,?,?,?,?,?,?)");
				pst.setLong(1, 562076608706723852L);
				pst.setLong(2, 562076320075694080L);
				pst.setLong(3, 562076608706723852L);
				pst.setString(4, "菜单管理");
				pst.setString(5, "");
				pst.setString(6, CodeConstant.OLD_THEME.equals(parameters.getThemeVal()) ? "cm_sys_menu/list" : "cmSysMenu/list.html");
				pst.setInt(7, 0);
				pst.setTimestamp(8, new Timestamp(System.currentTimeMillis()));
				pst.setLong(9, 1);
				pst.executeUpdate();
				pst.close();
				pst = connection.prepareStatement("INSERT INTO cm_sys_menu(menu_id,parent_id,order_no,menu_name,menu_icon,url_address,can_del,create_time,create_user_id) VALUES(?,?,?,?,?,?,?,?,?)");
				pst.setLong(1, 582393341447278592L);
				pst.setLong(2, 562076320075694080L);
				pst.setLong(3, 582393341447278592L);
				pst.setString(4, "角色管理");
				pst.setString(5, "");
				pst.setString(6, CodeConstant.OLD_THEME.equals(parameters.getThemeVal()) ? "cm_sys_role/list" : "cmSysRole/list.html");
				pst.setInt(7, 0);
				pst.setTimestamp(8, new Timestamp(System.currentTimeMillis()));
				pst.setLong(9, 1);
				pst.executeUpdate();
				pst.close();
			}

			if (!EXIST_TABLES.contains("cm_sys_role_menu")) {
				//初始化cm_sys_role_menu表
				pst = connection.prepareStatement("INSERT INTO cm_sys_role_menu(role_menu_id,role_id,menu_id,create_time,create_user_id) VALUES(?,?,?,?,?)");
				pst.setLong(1, 583197571498196992L);
				pst.setLong(2, 1);
				pst.setLong(3, 562076320075694080L);
				pst.setTimestamp(4, new Timestamp(System.currentTimeMillis()));
				pst.setLong(5, 1);
				pst.executeUpdate();
				pst.close();
				pst = connection.prepareStatement("INSERT INTO cm_sys_role_menu(role_menu_id,role_id,menu_id,create_time,create_user_id) VALUES(?,?,?,?,?)");
				pst.setLong(1, 583197571506585600L);
				pst.setLong(2, 1);
				pst.setLong(3, 562076608706723840L);
				pst.setTimestamp(4, new Timestamp(System.currentTimeMillis()));
				pst.setLong(5, 1);
				pst.executeUpdate();
				pst.close();
				pst = connection.prepareStatement("INSERT INTO cm_sys_role_menu(role_menu_id,role_id,menu_id,create_time,create_user_id) VALUES(?,?,?,?,?)");
				pst.setLong(1, 583197571523362816L);
				pst.setLong(2, 1);
				pst.setLong(3, 562076608706723852L);
				pst.setTimestamp(4, new Timestamp(System.currentTimeMillis()));
				pst.setLong(5, 1);
				pst.executeUpdate();
				pst.close();
				pst = connection.prepareStatement("INSERT INTO cm_sys_role_menu(role_menu_id,role_id,menu_id,create_time,create_user_id) VALUES(?,?,?,?,?)");
				pst.setLong(1, 583197571552722944L);
				pst.setLong(2, 1);
				pst.setLong(3, 582393341447278592L);
				pst.setTimestamp(4, new Timestamp(System.currentTimeMillis()));
				pst.setLong(5, 1);
				pst.executeUpdate();
				pst.close();
			}

			if (!EXIST_TABLES.contains("cm_sys_button")) {
				pst = connection.prepareStatement("INSERT INTO cm_sys_button(button_id,menu_id,button_name,module_tag_Id,create_time,create_user_id) VALUES(?,?,?,?,?,?)");
				pst.setLong(1, 583066629735907327L);
				pst.setLong(2, 562076608706723840L);
				pst.setString(3, "查询");
				pst.setString(4, "queryTag");
				pst.setTimestamp(5, new Timestamp(System.currentTimeMillis()));
				pst.setLong(6, 1);
				pst.executeUpdate();
				pst.close();
				//初始化cm_sys_button表
				pst = connection.prepareStatement("INSERT INTO cm_sys_button(button_id,menu_id,button_name,module_tag_Id,create_time,create_user_id) VALUES(?,?,?,?,?,?)");
				pst.setLong(1, 583166629735907328L);
				pst.setLong(2, 562076608706723840L);
				pst.setString(3, "添加");
				pst.setString(4, "addTag");
				pst.setTimestamp(5, new Timestamp(System.currentTimeMillis()));
				pst.setLong(6, 1);
				pst.executeUpdate();
				pst.close();
				pst = connection.prepareStatement("INSERT INTO cm_sys_button(button_id,menu_id,button_name,module_tag_Id,create_time,create_user_id) VALUES(?,?,?,?,?,?)");
				pst.setLong(1, 583166706466504704L);
				pst.setLong(2, 562076608706723840L);
				pst.setString(3, "更新");
				pst.setString(4, "upTag");
				pst.setTimestamp(5, new Timestamp(System.currentTimeMillis()));
				pst.setLong(6, 1);
				pst.executeUpdate();
				pst.close();
				pst = connection.prepareStatement("INSERT INTO cm_sys_button(button_id,menu_id,button_name,module_tag_Id,create_time,create_user_id) VALUES(?,?,?,?,?,?)");
				pst.setLong(1, 583166791195639808L);
				pst.setLong(2, 562076608706723840L);
				pst.setString(3, "删除");
				pst.setString(4, "delTag");
				pst.setTimestamp(5, new Timestamp(System.currentTimeMillis()));
				pst.setLong(6, 1);
				pst.executeUpdate();
				pst.close();
				pst = connection.prepareStatement("INSERT INTO cm_sys_button(button_id,menu_id,button_name,module_tag_Id,create_time,create_user_id) VALUES(?,?,?,?,?,?)");
				pst.setLong(1, 583068347689271295L);
				pst.setLong(2, 582393341447278592L);
				pst.setString(3, "查询");
				pst.setString(4, "queryTag");
				pst.setTimestamp(5, new Timestamp(System.currentTimeMillis()));
				pst.setLong(6, 1);
				pst.executeUpdate();
				pst.close();
				pst = connection.prepareStatement("INSERT INTO cm_sys_button(button_id,menu_id,button_name,module_tag_Id,create_time,create_user_id) VALUES(?,?,?,?,?,?)");
				pst.setLong(1, 583168347689271296L);
				pst.setLong(2, 582393341447278592L);
				pst.setString(3, "添加");
				pst.setString(4, "addTag");
				pst.setTimestamp(5, new Timestamp(System.currentTimeMillis()));
				pst.setLong(6, 1);
				pst.executeUpdate();
				pst.close();
				pst = connection.prepareStatement("INSERT INTO cm_sys_button(button_id,menu_id,button_name,module_tag_Id,create_time,create_user_id) VALUES(?,?,?,?,?,?)");
				pst.setLong(1, 583068436348469247L);
				pst.setLong(2, 582393341447278592L);
				pst.setString(3, "更新");
				pst.setString(4, "upTag");
				pst.setTimestamp(5, new Timestamp(System.currentTimeMillis()));
				pst.setLong(6, 1);
				pst.executeUpdate();
				pst.close();
				pst = connection.prepareStatement("INSERT INTO cm_sys_button(button_id,menu_id,button_name,module_tag_Id,create_time,create_user_id) VALUES(?,?,?,?,?,?)");
				pst.setLong(1, 583168436348469248L);
				pst.setLong(2, 582393341447278592L);
				pst.setString(3, "删除");
				pst.setString(4, "delTag");
				pst.setTimestamp(5, new Timestamp(System.currentTimeMillis()));
				pst.setLong(6, 1);
				pst.executeUpdate();
				pst.close();
				pst = connection.prepareStatement("INSERT INTO cm_sys_button(button_id,menu_id,button_name,module_tag_Id,create_time,create_user_id) VALUES(?,?,?,?,?,?)");
				pst.setLong(1, 583170351580921856L);
				pst.setLong(2, 562076608706723852L);
				pst.setString(3, "添加操作");
				pst.setString(4, "addButtonTag");
				pst.setTimestamp(5, new Timestamp(System.currentTimeMillis()));
				pst.setLong(6, 1);
				pst.executeUpdate();
				pst.close();
				pst = connection.prepareStatement("INSERT INTO cm_sys_button(button_id,menu_id,button_name,module_tag_Id,create_time,create_user_id) VALUES(?,?,?,?,?,?)");
				pst.setLong(1, 583170406463389696L);
				pst.setLong(2, 562076608706723852L);
				pst.setString(3, "删除");
				pst.setString(4, "delTag");
				pst.setTimestamp(5, new Timestamp(System.currentTimeMillis()));
				pst.setLong(6, 1);
				pst.executeUpdate();
				pst.close();
				pst = connection.prepareStatement("INSERT INTO cm_sys_button(button_id,menu_id,button_name,module_tag_Id,create_time,create_user_id) VALUES(?,?,?,?,?,?)");
				pst.setLong(1, 583170462281187328L);
				pst.setLong(2, 562076608706723852L);
				pst.setString(3, "添加二级菜单");
				pst.setString(4, "addSecondMenuTag");
				pst.setTimestamp(5, new Timestamp(System.currentTimeMillis()));
				pst.setLong(6, 1);
				pst.executeUpdate();
				pst.close();
				pst = connection.prepareStatement("INSERT INTO cm_sys_button(button_id,menu_id,button_name,module_tag_Id,create_time,create_user_id) VALUES(?,?,?,?,?,?)");
				pst.setLong(1, 583170592136839168L);
				pst.setLong(2, 562076608706723852L);
				pst.setString(3, "添加一级菜单");
				pst.setString(4, "addFirstMenuTag");
				pst.setTimestamp(5, new Timestamp(System.currentTimeMillis()));
				pst.setLong(6, 1);
				pst.executeUpdate();
				pst.close();
				pst = connection.prepareStatement("INSERT INTO cm_sys_button(button_id,menu_id,button_name,module_tag_Id,create_time,create_user_id) VALUES(?,?,?,?,?,?)");
				pst.setLong(1, 583170689817985024L);
				pst.setLong(2, 562076608706723852L);
				pst.setString(3, "保存");
				pst.setString(4, "upTag");
				pst.setTimestamp(5, new Timestamp(System.currentTimeMillis()));
				pst.setLong(6, 1);
				pst.executeUpdate();
				pst.close();
			}

			if (!EXIST_TABLES.contains("cm_sys_role_button")) {
				//初始化cm_sys_role_button表
				pst = connection.prepareStatement("INSERT INTO cm_sys_role_button(role_button_id,button_id,role_id,menu_id,create_time,create_user_id) VALUES(?,?,?,?,?,?)");
				pst.setLong(1, 583097571510779903L);
				pst.setLong(2, 583066629735907327L);
				pst.setLong(3, 1);
				pst.setLong(4, 562076608706723840L);
				pst.setTimestamp(5, new Timestamp(System.currentTimeMillis()));
				pst.setLong(6, 1);
				pst.executeUpdate();
				pst.close();
				pst = connection.prepareStatement("INSERT INTO cm_sys_role_button(role_button_id,button_id,role_id,menu_id,create_time,create_user_id) VALUES(?,?,?,?,?,?)");
				pst.setLong(1, 583197571510779904L);
				pst.setLong(2, 583166629735907328L);
				pst.setLong(3, 1);
				pst.setLong(4, 562076608706723840L);
				pst.setTimestamp(5, new Timestamp(System.currentTimeMillis()));
				pst.setLong(6, 1);
				pst.executeUpdate();
				pst.close();
				pst = connection.prepareStatement("INSERT INTO cm_sys_role_button(role_button_id,button_id,role_id,menu_id,create_time,create_user_id) VALUES(?,?,?,?,?,?)");
				pst.setLong(1, 583197571514974208L);
				pst.setLong(2, 583166706466504704L);
				pst.setLong(3, 1);
				pst.setLong(4, 562076608706723840L);
				pst.setTimestamp(5, new Timestamp(System.currentTimeMillis()));
				pst.setLong(6, 1);
				pst.executeUpdate();
				pst.close();
				pst = connection.prepareStatement("INSERT INTO cm_sys_role_button(role_button_id,button_id,role_id,menu_id,create_time,create_user_id) VALUES(?,?,?,?,?,?)");
				pst.setLong(1, 583197571519168512L);
				pst.setLong(2, 583166791195639808L);
				pst.setLong(3, 1);
				pst.setLong(4, 562076608706723840L);
				pst.setTimestamp(5, new Timestamp(System.currentTimeMillis()));
				pst.setLong(6, 1);
				pst.executeUpdate();
				pst.close();
				pst = connection.prepareStatement("INSERT INTO cm_sys_role_button(role_button_id,button_id,role_id,menu_id,create_time,create_user_id) VALUES(?,?,?,?,?,?)");
				pst.setLong(1, 583197571527557120L);
				pst.setLong(2, 583170351580921856L);
				pst.setLong(3, 1);
				pst.setLong(4, 562076608706723852L);
				pst.setTimestamp(5, new Timestamp(System.currentTimeMillis()));
				pst.setLong(6, 1);
				pst.executeUpdate();
				pst.close();
				pst = connection.prepareStatement("INSERT INTO cm_sys_role_button(role_button_id,button_id,role_id,menu_id,create_time,create_user_id) VALUES(?,?,?,?,?,?)");
				pst.setLong(1, 583197571535945728L);
				pst.setLong(2, 583170406463389696L);
				pst.setLong(3, 1);
				pst.setLong(4, 562076608706723852L);
				pst.setTimestamp(5, new Timestamp(System.currentTimeMillis()));
				pst.setLong(6, 1);
				pst.executeUpdate();
				pst.close();
				pst = connection.prepareStatement("INSERT INTO cm_sys_role_button(role_button_id,button_id,role_id,menu_id,create_time,create_user_id) VALUES(?,?,?,?,?,?)");
				pst.setLong(1, 583197571535945729L);
				pst.setLong(2, 583170462281187328L);
				pst.setLong(3, 1);
				pst.setLong(4, 562076608706723852L);
				pst.setTimestamp(5, new Timestamp(System.currentTimeMillis()));
				pst.setLong(6, 1);
				pst.executeUpdate();
				pst.close();
				pst = connection.prepareStatement("INSERT INTO cm_sys_role_button(role_button_id,button_id,role_id,menu_id,create_time,create_user_id) VALUES(?,?,?,?,?,?)");
				pst.setLong(1, 583197571540140032L);
				pst.setLong(2, 583170592136839168L);
				pst.setLong(3, 1);
				pst.setLong(4, 562076608706723852L);
				pst.setTimestamp(5, new Timestamp(System.currentTimeMillis()));
				pst.setLong(6, 1);
				pst.executeUpdate();
				pst.close();
				pst = connection.prepareStatement("INSERT INTO cm_sys_role_button(role_button_id,button_id,role_id,menu_id,create_time,create_user_id) VALUES(?,?,?,?,?,?)");
				pst.setLong(1, 583197571548528640L);
				pst.setLong(2, 583170689817985024L);
				pst.setLong(3, 1);
				pst.setLong(4, 562076608706723852L);
				pst.setTimestamp(5, new Timestamp(System.currentTimeMillis()));
				pst.setLong(6, 1);
				pst.executeUpdate();
				pst.close();
				pst = connection.prepareStatement("INSERT INTO cm_sys_role_button(role_button_id,button_id,role_id,menu_id,create_time,create_user_id) VALUES(?,?,?,?,?,?)");
				pst.setLong(1, 583097571561111551L);
				pst.setLong(2, 583068347689271295L);
				pst.setLong(3, 1);
				pst.setLong(4, 582393341447278592L);
				pst.setTimestamp(5, new Timestamp(System.currentTimeMillis()));
				pst.setLong(6, 1);
				pst.executeUpdate();
				pst.close();
				pst = connection.prepareStatement("INSERT INTO cm_sys_role_button(role_button_id,button_id,role_id,menu_id,create_time,create_user_id) VALUES(?,?,?,?,?,?)");
				pst.setLong(1, 583197571561111552L);
				pst.setLong(2, 583168347689271296L);
				pst.setLong(3, 1);
				pst.setLong(4, 582393341447278592L);
				pst.setTimestamp(5, new Timestamp(System.currentTimeMillis()));
				pst.setLong(6, 1);
				pst.executeUpdate();
				pst.close();
				pst = connection.prepareStatement("INSERT INTO cm_sys_role_button(role_button_id,button_id,role_id,menu_id,create_time,create_user_id) VALUES(?,?,?,?,?,?)");
				pst.setLong(1, 583097571565305855L);
				pst.setLong(2, 583068436348469247L);
				pst.setLong(3, 1);
				pst.setLong(4, 582393341447278592L);
				pst.setTimestamp(5, new Timestamp(System.currentTimeMillis()));
				pst.setLong(6, 1);
				pst.executeUpdate();
				pst.close();
				pst = connection.prepareStatement("INSERT INTO cm_sys_role_button(role_button_id,button_id,role_id,menu_id,create_time,create_user_id) VALUES(?,?,?,?,?,?)");
				pst.setLong(1, 583197571565305856L);
				pst.setLong(2, 583168436348469248L);
				pst.setLong(3, 1);
				pst.setLong(4, 582393341447278592L);
				pst.setTimestamp(5, new Timestamp(System.currentTimeMillis()));
				pst.setLong(6, 1);
				pst.executeUpdate();
				pst.close();
			}

			//如果是多数据源模式
			if (parameters.isMutiDataSource()) {
				//遍历每个数据源
				for (Map.Entry<String, DataSourceModel> modelEntry : ChildWindowConstant.dataSourceModelMap.entrySet()) {
					DataSourceModel dataSourceModel = modelEntry.getValue();
					insertCustomData(parameters, dataSourceModel.getCurrentTableCnNameMap(), dataSourceModel.getTablesQueryMap(), dataSourceModel.getTablesQueryEndAndCnMap(), connection);
				}
			} else {
				insertCustomData(parameters, ChildWindowConstant.currentTableCnNameMap, ChildWindowConstant.tablesQueryMap, ChildWindowConstant.tablesQueryEndAndCnMap, connection);
			}

		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, "权限管理初始化数据时出错（mysql请使用5.7及以上版本）：" + e.getMessage(), "错误",
					JOptionPane.ERROR_MESSAGE);
		} finally {
			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException ignored) {
				}
			}
		}
	}

	/**
	 * 插入自己定义的菜单等数据
	 */
	private static void insertCustomData(Parameters parameters, Map<String, String> currentTableCnNameMap, Map<String, Map<String, TablesQueryModel>> tablesQueryMap, Map<String, String> tablesQueryEndAndCnMap, Connection connection) throws SQLException {
		PreparedStatement pst;
		//添加自定义的菜单
		List<CmSysMenu> cmSysMenus = getCmSysMenus(parameters, currentTableCnNameMap, tablesQueryMap, tablesQueryEndAndCnMap);
		//按钮列表
		List<CmSysButton> cmSysButtons = new ArrayList<>(10);
		List<Long> menuIds = new ArrayList<>();
		//插入cm_sys_menu表
		for (CmSysMenu cmSysMenu : cmSysMenus) {
			if (cmSysMenu.getParentId() == 0) {
				//判断是否存在该菜单，存在，则不添加菜单数据
				pst = connection.prepareStatement("SELECT menu_id FROM cm_sys_menu WHERE menu_name=?");
				pst.setString(1, cmSysMenu.getMenuName());
				ResultSet rs = pst.executeQuery();
				if (rs.next()) {
					menuIds.add(cmSysMenu.getMenuId());
					continue;
				}
			}
			if (menuIds.contains(cmSysMenu.getParentId())) {
				continue;
			}
			pst = connection.prepareStatement("INSERT INTO cm_sys_menu(menu_id,parent_id,order_no,menu_name,menu_icon,url_address,create_time,create_user_id) VALUES(?,?,?,?,?,?,?,?)");
			pst.setLong(1, cmSysMenu.getMenuId());
			pst.setLong(2, cmSysMenu.getParentId());
			pst.setLong(3, cmSysMenu.getOrderNo());
			pst.setString(4, cmSysMenu.getMenuName());
			pst.setString(5, cmSysMenu.getMenuIcon());
			pst.setString(6, cmSysMenu.getUrlAddress());
			pst.setTimestamp(7, new Timestamp(System.currentTimeMillis()));
			pst.setLong(8, 1);
			pst.executeUpdate();
			pst.close();
			//插入cm_sys_role_menu表
			pst = connection.prepareStatement("INSERT INTO cm_sys_role_menu(role_menu_id,role_id,menu_id,create_time,create_user_id) VALUES(?,?,?,?,?)");
			pst.setLong(1, SnowflakeIdWorker.generateId());
			pst.setLong(2, 1);
			pst.setLong(3, cmSysMenu.getMenuId());
			pst.setTimestamp(4, new Timestamp(System.currentTimeMillis()));
			pst.setLong(5, 1);
			pst.executeUpdate();
			pst.close();
			//插入cm_sys_button表（需要在模板标签上添加moduleTagId属性）
			//如果是二级菜单，则插入按钮
			if (cmSysMenu.getParentId() != 0) {
				//如果是单表查询，插入增删改按钮
				if (!cmSysMenu.getUrlAddress().contains("Muti")) {
					//查询
					CmSysButton cmSysButtonQuery = new CmSysButton();
					cmSysButtonQuery.setButtonId(SnowflakeIdWorker.generateId());
					cmSysButtonQuery.setMenuId(cmSysMenu.getMenuId());
					cmSysButtonQuery.setButtonName("查询");
					cmSysButtonQuery.setModuleTagId("queryTag");
					//添加按钮
					CmSysButton cmSysButtonAdd = new CmSysButton();
					cmSysButtonAdd.setButtonId(SnowflakeIdWorker.generateId());
					cmSysButtonAdd.setMenuId(cmSysMenu.getMenuId());
					cmSysButtonAdd.setButtonName("添加");
					cmSysButtonAdd.setModuleTagId("addTag");
					//更新按钮
					CmSysButton cmSysButtonUpdate = new CmSysButton();
					cmSysButtonUpdate.setButtonId(SnowflakeIdWorker.generateId());
					cmSysButtonUpdate.setMenuId(cmSysMenu.getMenuId());
					cmSysButtonUpdate.setButtonName("更新");
					cmSysButtonUpdate.setModuleTagId("upTag");
					//删除按钮
					CmSysButton cmSysButtonDelete = new CmSysButton();
					cmSysButtonDelete.setButtonId(SnowflakeIdWorker.generateId());
					cmSysButtonDelete.setMenuId(cmSysMenu.getMenuId());
					cmSysButtonDelete.setButtonName("删除");
					cmSysButtonDelete.setModuleTagId("delTag");
					//加入列表
					cmSysButtons.add(cmSysButtonQuery);
					cmSysButtons.add(cmSysButtonAdd);
					cmSysButtons.add(cmSysButtonUpdate);
					cmSysButtons.add(cmSysButtonDelete);
				}
			}
		}
		//插入cm_sys_button表
		for (CmSysButton cmSysButton : cmSysButtons) {
			pst = connection.prepareStatement("INSERT INTO cm_sys_button(button_id,menu_id,button_name,module_tag_Id,create_time,create_user_id) VALUES(?,?,?,?,?,?)");
			pst.setLong(1, cmSysButton.getButtonId());
			pst.setLong(2, cmSysButton.getMenuId());
			pst.setString(3, cmSysButton.getButtonName());
			pst.setString(4, cmSysButton.getModuleTagId());
			pst.setTimestamp(5, new Timestamp(System.currentTimeMillis()));
			pst.setLong(6, 1);
			pst.executeUpdate();
			pst.close();
			//插入cm_sys_role_button表
			pst = connection.prepareStatement("INSERT INTO cm_sys_role_button(role_button_id,button_id,role_id,menu_id,create_time,create_user_id) VALUES(?,?,?,?,?,?)");
			pst.setLong(1, SnowflakeIdWorker.generateId());
			pst.setLong(2, cmSysButton.getButtonId());
			pst.setLong(3, 1);
			pst.setLong(4, cmSysButton.getMenuId());
			pst.setTimestamp(5, new Timestamp(System.currentTimeMillis()));
			pst.setLong(6, 1);
			pst.executeUpdate();
			pst.close();
		}
	}

	public static List<CmSysMenu> getCmSysMenus(Parameters parameters, Map<String, String> currentTableCnNameMap, Map<String, Map<String, TablesQueryModel>> tablesQueryMap, Map<String, String> tablesQueryEndAndCnMap) {
		List<CmSysMenu> cmSysMenus = new ArrayList<>(10);
		//查询所有单表 一个单表默认为一个一级菜单，同时包含一个二级子菜单
		for (Map.Entry<String, String> entry : currentTableCnNameMap.entrySet()) {
			//获取表英文名
			String tableEng = entry.getKey();
			//获取表中文名
			String tableCn = entry.getValue();
			//组装menu
			//一级菜单
			CmSysMenu menuParent = new CmSysMenu();
			menuParent.setMenuId(SnowflakeIdWorker.generateId());
			menuParent.setMenuName(tableCn);
			menuParent.setOrderNo(menuParent.getMenuId());
			menuParent.setParentId(0L);
			menuParent.setMenuIcon(CodeConstant.OLD_THEME.equals(parameters.getThemeVal()) ? "icon icon-chart" : "fa fa fa-bar-chart-o");
			menuParent.setTableEng(tableEng);
			menuParent.setLevel(MenuLevel.PARENT);
			//二级地址菜单
			CmSysMenu menuChild = new CmSysMenu();
			menuChild.setMenuId(SnowflakeIdWorker.generateId());
			menuChild.setParentId(menuParent.getMenuId());
			menuChild.setOrderNo(menuChild.getMenuId());
			menuChild.setMenuName("信息管理");
			menuChild.setUrlAddress(tableEng + (CodeConstant.OLD_THEME.equals(parameters.getThemeVal()) ? "/list" : "/list.html"));
			menuParent.setTableEng(tableEng);
			menuParent.setLevel(MenuLevel.CHILD);
			//添加到cmSysMenus中
			cmSysMenus.add(menuParent);
			cmSysMenus.add(menuChild);
		}
		//查询所有多表 模块名为一级菜单 里面的map 每个map里的方法为一个二级菜单
		for (Map.Entry<String, Map<String, TablesQueryModel>> entry : tablesQueryMap.entrySet()) {
			//获取模块英文名
			String modelNameEng = entry.getKey();
			//获取模块中文名
			String modelNameCn = tablesQueryEndAndCnMap.get(modelNameEng);
			//添加模块一级菜单
			CmSysMenu menuParent = new CmSysMenu();
			menuParent.setMenuId(SnowflakeIdWorker.generateId());
			menuParent.setParentId(0L);
			menuParent.setMenuName(modelNameCn);
			menuParent.setOrderNo(menuParent.getMenuId());
			menuParent.setMenuIcon(CodeConstant.OLD_THEME.equals(parameters.getThemeVal()) ? "icon icon-chart" : "fa fa fa-bar-chart-o");
			menuParent.setTableEng(modelNameEng);
			menuParent.setLevel(MenuLevel.PARENT);
			//添加到cmSysMenus中
			cmSysMenus.add(menuParent);
			//获取模块下的所有方法，添加二级菜单
			Map<String, TablesQueryModel> methodMap = entry.getValue();
			for (Map.Entry<String, TablesQueryModel> methodEntry : methodMap.entrySet()) {
				//添加二级菜单
				CmSysMenu menuChild = new CmSysMenu();
				menuChild.setMenuId(SnowflakeIdWorker.generateId());
				menuChild.setParentId(menuParent.getMenuId());
				menuChild.setOrderNo(menuChild.getMenuId());
				menuChild.setMenuName(methodEntry.getValue().getMethodName_cn());
				menuChild.setUrlAddress(modelNameEng + "Muti/" + methodEntry.getKey() + (CodeConstant.OLD_THEME.equals(parameters.getThemeVal()) ? "List" : "List.html"));
				menuChild.setTableEng(modelNameEng);
				menuParent.setLevel(MenuLevel.CHILD);
				//添加到cmSysMenus中
				cmSysMenus.add(menuChild);
			}
		}
		if (parameters.isCloudModel()) {
			List<CmSysMenu> cloudMenus = parameters.getCloudMenus();
			cmSysMenus.forEach(menu -> {
				if (!cloudMenus.contains(menu)) {
					cloudMenus.add(menu);
				}
			});
			return cloudMenus;
		}
		return cmSysMenus;
	}
}
