package constant;

public class CodeConstant {

	public final static String PLEASE_CHOOSE = "--请选择--";

	public final static String NEW_LINE = System.getProperty("line.separator");

	public final static String MYSQL = "mysql";

	public final static String ORACLE = "oracle";

	public final static String POSTGRESQL = "postgresql";

	public final static String SQL_SERVER = "sqlserver";

	public static final String MAKE_ENTITY_MODELS = "MakeEntityModels";

	public static final String CAPTURE_PACKAGE_NAME = "capturePackageName";

	public static final String CAPTURE_PROJECT_NAME = "captureProjectName";

	public static final String PACKAGE_NAME = "packageName";

	public static final String PROJECT_NAME = "projectName";

	public static final String PROJECT_CN_NAME = "projectCnName";

	public static final String IDAO_NAME = "IDaoName";

	public static final String ISERVICE_NAME = "IServiceName";

	public static final String SERVICE_NAME = "serviceName";

	public static final String CONTROLLER_NAME = "controllerName";

	public static final String CONTROLLER_PREFIX = "controllerPrefix";

	public static final String DATA_BASE_USER_NAME = "dataBaseUserName";

	public static final String DATA_BASE_PWD = "dataBasePwd";

	public static final String DATA_BASE_URL = "dataBaseUrl";

	public static final String DATA_BASE_TYPE = "dataBaseType";

	public static final String DATA_BASE_DRIVER_CLASS = "dataBaseDriverClass";

	public static final String TABLE_NAME = "tableName";

	public static final String REAL_TABLE_NAME = "realTableName";

	public static final String COLUMN_DATA = "columnData";

	public static final String COLUMN_LIST = "columnList";

	public static final String SELECT_COLUMN_LIST = "selectColumnList";

	public static final String UPDATE_COLUMN_LIST = "updateColumnList";

	public static final String SELECT_COLUMN_LIST_MAPPER = "selectColumnListMapper";

	public static final String PRIMARY_KEY_LIST = "primaryKeyList";

	public static final String TABLE_NAME_LIST = "tableNameList";

	public static final String QUERY_COLUMN_LIST = "queryColumnList";

	public static final String CURRENT_TABLE_CN_NAME = "currentTableCnName";

	public static final String FRAME_WORK_VAL = "frameWorkVal";

	public static final String TABLES_QUERY_MAP = "tablesQueryMap";

	public static final String TABLES_QUERY_END_AND_CN_MAP = "tablesQueryEndAndCnMap";

	public static final String CURRENT_MUTI_ENG = "currentMutiEng";

	public static final String CURRENT_MUTI_CN = "currentMutiCn";

	public static final String CURRENT_MUTI_METHOD_ENG = "currentMutiMethodEng";

	public static final String CURRENT_MUTI_METHOD_CN = "currentMutiMethodCn";

	public static final String TABLE_FILED_MODELS = "tableFiledModels";

	public static final String TABLE_CONDITION_MODELS = "tableConditionModels";

	public static final String JS_FRAME_WORK = "jsFrameWork";

	public static final String THEME = "theme";

	public static final String CLIENT_STYLE_VAL = "clientStyleVal";

	public static final String USER_CONDITION = "userCondition";

	public static final String USER_TABLE = "userTable";

	public static final String USER_NAME_FILED = "userNameFiled";

	public static final String USER_PWD_FILED = "userPwdFiled";

	public static final String LOGIN_MODEL = "loginModel";

	public static final String ENTITY_NAME = "entityName";

	public static final String ENTITY_NAME_AND_TYPES = "entityNameAndTypes";

	public static final String PARAM_CONFIG_KEY = "paramConfigKey";

	public static final String OLD_THEME = "经典后台Thymleaf版";

	public static final String H_ADMIN_THEME = "前后端分离响应式";

	public static final String DATABASE_POOL = "databasePool";

	public static final String IF_USE_SWAGGER = "ifUseSwagger";

	public static final String IS_AUTHORITY = "isAuthority";

	public static final String IS_CONTROLLER = "isController";

	public static final String IS_SERVICE = "isService";

	public static final String IS_DAO = "isDao";

	public static final String IS_ENTITY = "isEntity";

	public static final String IS_VIEW = "isView";

	public static final String DATA_SOURCE_NAME = "dataSourceName";

	public static final String IS_MUTI_DATA_SOURCE = "isMutiDataSource";

	public static final String CURRENT_METHOD_MAP = "currentMethodMap";

	public static final String CAP_CURRENT_MUTI_ENG = "capCurrentMutiEng";

	public static final String CURRENT_MUTI_ENTITY_NAME = "currentMutiEntityName";

	public static final String CURRENT_MUTI_ENTITY_NAME_CN = "currentMutiEntityNameCn";

	public static final String ENTITY_FILED_MODELS = "entityFiledModels";

	public static final String MAKE_ENTITY_NAME = "makeEntityName";

	public static final String MAKE_ENTITY_NAME_CN = "makeEntityName_cn";

	public static final String HAS_LIST = "hasList";

	public static final String PRIMARY_KEY_MODEL_LIST = "primaryKeyModelList";

	public static final String DATA_SOURCE_MODEL_MAP = "dataSourceModelMap";

	public static final String CURRENT_DATA_SOURCE_NAME = "currentDataSourceName";

	public static final String EUREKA = "eureka";

	public static final String NACOS = "nacos";

	public static final String ZUUL = "zuul";

	public static final String GATEWAY = "gateway";

	public static final String IS_CLOUD_MODEL = "isCloudModel";

	public static final String CLOUD_SERVICES = "cloudServices";

	public static final String CLOUD_MENUS = "cloudMenus";

	public static final String CLOUD_SYS_DS = "cloudSysDs";

	public static final String CLOUD_SYS_NAME = "cloudSysName";

	public static final String CLOUD_REGISTE_CENTER = "cloudRegisteCenter";

	public static final String CLOUD_SYS_ENG_NAME = "cloudSysEngName";

	public static final String IS_REDIS_SINGLE_LOGIN = "isRedisSingleLogin";

	public static final String IS_ONLY_CLOUD_MODEL = "isOnlyCloudModel";

	public static final String IS_COVER_EXIST_FILE = "isCoverExistFile";
}
