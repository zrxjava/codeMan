package <#if isCloudModel>${cloudSysEngName}<#else>${packageName}</#if>.service;

<#if !entityName??>
import java.util.Map;
<#else>
import <#if isCloudModel>${cloudSysEngName}<#else>${packageName}</#if>.entity.PageData;
</#if>
import java.util.List;


<#if !entityName??>
public interface IBaseService {

	void add(Map<String, Object> map);

	void delete(Map<String, Object> map);

	void update(Map<String, Object> map);

	List<Map<String,Object>> select(Map<String, Object> map);

	Map<String, Object> likeSelect(Map<String, Object> map);

	void batchAdd(List<Map<String, Object>> list);

	void batchDelete(List<Map<String, Object>> list);

	void batchUpdate(List<Map<String, Object>> list);

}
<#else>
public interface IBaseService<E> {

	void add(E entity);

	void delete(E entity);

	void update(E entity);

	List<E> select(E entity);

	PageData<E> likeSelect(E entity);

	void batchAdd(List<E> list);

	void batchDelete(List<E> list);

	void batchUpdate(List<E> list);

}
</#if>
