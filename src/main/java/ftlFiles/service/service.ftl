package <#if isCloudModel>${cloudSysEngName}<#else>${packageName}</#if>.service.impl;

<#if currentDataSourceName??>
import <#if isCloudModel>${cloudSysEngName}<#else>${packageName}</#if>.config.mutidatasource.DBType;
import <#if isCloudModel>${cloudSysEngName}<#else>${packageName}</#if>.config.mutidatasource.DataSourceType;
</#if>
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import <#if isCloudModel>${cloudSysEngName}<#else>${packageName}</#if>.dao.${IDaoName};
import <#if isCloudModel>${cloudSysEngName}<#else>${packageName}</#if>.service.${IServiceName};
import <#if isCloudModel>${cloudSysEngName}<#else>${packageName}</#if>.entity.PageData;
import <#if isCloudModel>${cloudSysEngName}<#else>${packageName}</#if>.utils.ExcelUtil;
import <#if isCloudModel>${cloudSysEngName}<#else>${packageName}</#if>.utils.PageUtil;
<#if entityName??>
import java.util.LinkedHashMap;
import <#if isCloudModel>${cloudSysEngName}<#else>${packageName}</#if>.entity.${entityName};
</#if>
import java.util.List;
import java.util.Map;

@Service
<#if !entityName??>
public class ${serviceName} implements ${IServiceName} {


	private final ${IDaoName} dao;

	@Autowired
	public ${serviceName}(${IDaoName} dao) {
		this.dao = dao;
	}

<#if currentDataSourceName??>
    @DBType(DataSourceType.${currentDataSourceName?upper_case})
</#if>
	@Override
	public void add(Map<String, Object> map) {
		dao.add(map);
	}

<#if currentDataSourceName??>
    @DBType(DataSourceType.${currentDataSourceName?upper_case})
</#if>
	@Override
	public void delete(Map<String, Object> map) {
		dao.delete(map);
	}

<#if currentDataSourceName??>
    @DBType(DataSourceType.${currentDataSourceName?upper_case})
</#if>
	@Override
	public void update(Map<String, Object> map) {
		dao.update(map);
	}

<#if currentDataSourceName??>
    @DBType(DataSourceType.${currentDataSourceName?upper_case})
</#if>
	@Override
	public List<Map<String,Object>> select(Map<String, Object> map) {
		return dao.select(map);
	}

<#if currentDataSourceName??>
    @DBType(DataSourceType.${currentDataSourceName?upper_case})
</#if>
	@Override
	public Map<String, Object> likeSelect(Map<String, Object> map) {

		return PageUtil.getPageData(map, dao);
	}

<#if currentDataSourceName??>
    @DBType(DataSourceType.${currentDataSourceName?upper_case})
</#if>
	@Override
	public void exportExcel(Map<String, Object> paramMap, HttpServletResponse response) {

		// 获取头部信息
		String[] headList = new String[] {<#list selectColumnList as data> "${data.columnsCn}"<#if data_has_next>,</#if></#list>};

		String[] describeList = new String[] {<#list selectColumnList as data> "${data.serviceTextStr}"<#if data_has_next>,</#if></#list>};

		ExcelUtil.exportExcel(paramMap, response, dao, headList, describeList);
	}
}
<#else>
public class ${serviceName} implements ${IServiceName} {


	private final ${IDaoName} dao;

	@Autowired
	public ${serviceName}(${IDaoName} dao) {
		this.dao = dao;
	}

<#if currentDataSourceName??>
    @DBType(DataSourceType.${currentDataSourceName?upper_case})
</#if>
	@Override
	public void add(${entityName} entity) {
		dao.add(entity);
	}

<#if currentDataSourceName??>
    @DBType(DataSourceType.${currentDataSourceName?upper_case})
</#if>
	@Override
	public void delete(${entityName} entity) {
		dao.delete(entity);
	}

<#if currentDataSourceName??>
    @DBType(DataSourceType.${currentDataSourceName?upper_case})
</#if>
	@Override
	public void update(${entityName} entity) {
		dao.update(entity);
	}

<#if currentDataSourceName??>
    @DBType(DataSourceType.${currentDataSourceName?upper_case})
</#if>
	@Override
	public List<${entityName}> select(${entityName} entity) {
		return dao.select(entity);
	}

<#if currentDataSourceName??>
    @DBType(DataSourceType.${currentDataSourceName?upper_case})
</#if>
	@Override
	public PageData<${entityName}> likeSelect(${entityName} entity) {
		return PageUtil.getPageData(entity, dao);
	}

<#if currentDataSourceName??>
    @DBType(DataSourceType.${currentDataSourceName?upper_case})
</#if>
	@Override
	public void exportExcel(${entityName} entity, HttpServletResponse response) {

		// 获取头部信息（可以设置为动态）
		String[] headList = new String[] {<#list selectColumnList as data> "${data.columnsCn}"<#if data_has_next>,</#if></#list>};

		String[] headEngList = new String[]{<#list selectColumnList as data> "${data.sqlParamColumnEng}"<#if data_has_next>,</#if></#list>};

		String[] describeList = new String[] {<#list selectColumnList as data> "${data.serviceTextStr}"<#if data_has_next>,</#if></#list>};

		//设置头部以及描述信息
        Map<String, String> headAndDescribeMap = new LinkedHashMap<>();
        for (int i = 0; i < headEngList.length; i++) {
            headAndDescribeMap.put(headEngList[i], describeList[i]);
        }

		ExcelUtil.exportExcel(entity, response, dao, headList, headAndDescribeMap);
	}
}
</#if>
