<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
	<modelVersion>4.0.0</modelVersion>

    <#if isCloudModel>
        <parent>
            <artifactId>sys-parent</artifactId>
            <groupId>cloud</groupId>
            <version>1.0-SNAPSHOT</version>
        </parent>
        <artifactId>service-${projectName}</artifactId>

        <dependencies>
          <#if cloudRegisteCenter=="eureka">
            <dependency>
                <groupId>org.springframework.cloud</groupId>
                <artifactId>spring-cloud-starter-netflix-eureka-client</artifactId>
            </dependency>
          </#if>
          <#if cloudRegisteCenter=="nacos">
             <dependency>
                    <groupId>com.alibaba.cloud</groupId>
                    <artifactId>spring-cloud-starter-alibaba-nacos-discovery</artifactId>
             </dependency>
          </#if>
            <!--引入公共组件-->
            <dependency>
                <groupId>cloud</groupId>
                <artifactId>sys-common</artifactId>
                <version>1.0-SNAPSHOT</version>
            </dependency>
            <!--引入feign-->
            <dependency>
                <groupId>cloud</groupId>
                <artifactId>sys-feign-api</artifactId>
                <version>1.0-SNAPSHOT</version>
            </dependency>
        </dependencies>
    </#if>
	<#if frameWorkVal=="springBoot" && !isCloudModel>
	<parent>
		<groupId>org.springframework.boot</groupId>
		<artifactId>spring-boot-starter-parent</artifactId>
        <version>2.2.6.RELEASE</version>
		<relativePath/> <!-- lookup parent from repository -->
	</parent>
	<groupId>codeMan</groupId>
	<artifactId>${projectName}</artifactId>
	<version>1.0.0.RELEASE</version>
	<name>${projectName}</name>
	<description>${projectName} for Spring Boot</description>

	<properties>
		<java.version>1.8</java.version>
	</properties>

	<dependencies>
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-web</artifactId>
		</dependency>

		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-aop</artifactId>
		</dependency>

		<!-- thymeleaf 模板引擎 -->
		<dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-thymeleaf</artifactId>
        </dependency>

     <#if isMutiDataSource>
        <!-- 数据库相关配置 根据需要自行选择-->
        <dependency>
            <groupId>mysql</groupId>
            <artifactId>mysql-connector-java</artifactId>
            <scope>runtime</scope>
        </dependency>
        <dependency>
            <groupId>org.postgresql</groupId>
            <artifactId>postgresql</artifactId>
            <scope>runtime</scope>
        </dependency>
        <dependency>
            <groupId>com.ojdbc</groupId>
            <artifactId>ojdbc</artifactId>
            <version>1.0.0</version>
            <scope>system</scope>
            <systemPath><#noparse>${</#noparse>project.basedir<#noparse>}</#noparse>/lib/ojdbc-11.2.0.3.0.jar</systemPath>
        </dependency>
        <dependency>
             <groupId>com.microsoft.sqlserver</groupId>
             <artifactId>sqljdbc6.0</artifactId>
             <version>6.0</version>
             <scope>system</scope>
             <systemPath><#noparse>${</#noparse>project.basedir<#noparse>}</#noparse>/lib/sqljdbc42.jar</systemPath>
        </dependency>
     <#else>
        <#if dataBaseType == "mysql">
        <!-- 数据库相关配置 -->
        <dependency>
            <groupId>mysql</groupId>
            <artifactId>mysql-connector-java</artifactId>
            <scope>runtime</scope>
        </dependency>
        </#if>

        <#if dataBaseType == "postgresql">
        <!-- 数据库相关配置 -->
        <dependency>
            <groupId>org.postgresql</groupId>
            <artifactId>postgresql</artifactId>
            <scope>runtime</scope>
        </dependency>
        </#if>

        <#if dataBaseType == "oracle">
        <dependency>
            <groupId>com.ojdbc</groupId>
            <artifactId>ojdbc</artifactId>
            <version>1.0.0</version>
            <scope>system</scope>
            <systemPath><#noparse>${</#noparse>project.basedir<#noparse>}</#noparse>/lib/ojdbc-11.2.0.3.0.jar</systemPath>
        </dependency>
        </#if>

         <#if dataBaseType == "sqlserver">
         <dependency>
             <groupId>com.microsoft.sqlserver</groupId>
             <artifactId>sqljdbc6.0</artifactId>
             <version>6.0</version>
             <scope>system</scope>
             <systemPath><#noparse>${</#noparse>project.basedir<#noparse>}</#noparse>/lib/sqljdbc42.jar</systemPath>
         </dependency>
         </#if>
     </#if>
     <#if isRedisSingleLogin>
         <!--redis-->
         <dependency>
             <groupId>org.springframework.boot</groupId>
             <artifactId>spring-boot-starter-data-redis</artifactId>
         </dependency>
         <!--commons-pool2-->
         <dependency>
             <groupId>org.apache.commons</groupId>
             <artifactId>commons-pool2</artifactId>
         </dependency>
     </#if>
        <dependency>
            <groupId>org.mybatis.spring.boot</groupId>
            <artifactId>mybatis-spring-boot-starter</artifactId>
            <version>1.3.1</version>
        </dependency>

		<#if databasePool == "Druid">
		<dependency>
			<groupId>com.alibaba</groupId>
			<artifactId>druid-spring-boot-starter</artifactId>
			<version>1.1.10</version>
		</dependency>
		</#if>

		<#if ifUseSwagger == "是">
		<dependency>
            <groupId>io.springfox</groupId>
            <artifactId>springfox-swagger2</artifactId>
            <version>2.9.2</version>
        </dependency>
        <dependency>
            <groupId>io.springfox</groupId>
            <artifactId>springfox-swagger-ui</artifactId>
            <version>2.9.2</version>
        </dependency>
        <dependency>
            <groupId>com.github.xiaoymin</groupId>
            <artifactId>swagger-bootstrap-ui</artifactId>
            <version>1.9.5</version>
        </dependency>
        </#if>

		<!-- spring boot devtools 依赖包. 热部署-->
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-devtools</artifactId>
			<optional>true</optional>
		</dependency>

		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-configuration-processor</artifactId>
			<optional>true</optional>
		</dependency>

        <dependency>
            <groupId>org.apache.poi</groupId>
            <artifactId>poi</artifactId>
            <version>4.0.1</version>
        </dependency>
		<dependency>
			<groupId>org.apache.poi</groupId>
			<artifactId>poi-ooxml</artifactId>
			<version>4.0.1</version>
		</dependency>

        <!-- lombok-->
        <dependency>
            <groupId>org.projectlombok</groupId>
            <artifactId>lombok</artifactId>
            <version>1.18.4</version>
        </dependency>

        <!--apache-->
        <dependency>
            <groupId>org.apache.commons</groupId>
            <artifactId>commons-lang3</artifactId>
            <version>3.8.1</version>
        </dependency>

		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-test</artifactId>
			<scope>test</scope>
		</dependency>

	</dependencies>

	<build>
		<plugins>
			<plugin>
				<groupId>org.springframework.boot</groupId>
				<artifactId>spring-boot-maven-plugin</artifactId>
				<configuration>
					<includeSystemScope>true</includeSystemScope>
				</configuration>
			</plugin>
		</plugins>
	</build>
	</#if>

	<#if frameWorkVal=="ssm">
	<groupId>codeMan</groupId>
	<artifactId>${projectName}</artifactId>
	<packaging>war</packaging>
	<version>1.0.0.RELEASE</version>
	<name>${projectName}</name>
	<url>http://maven.apache.org</url>

	<properties>
		<project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
		<!-- libs -->
		<junit.version>4.11</junit.version>
		<jstl.version>1.2</jstl.version>
		<javaee-api.version>7.0</javaee-api.version>
		<cglib.version>3.2.2</cglib.version>
		<aspectjrt.version>1.8.0</aspectjrt.version>
		<aspectjweaver.version>1.8.0</aspectjweaver.version>
		<spring.version>5.2.5.RELEASE</spring.version>
		<mybatis-spring.version>1.3.0</mybatis-spring.version>
		<mybatis.version>3.4.0</mybatis.version>
		<log4j.version>1.2.17</log4j.version>
		<slf4j.version>1.7.21</slf4j.version>
		<fileupload.version>1.3.1</fileupload.version>
        <lombok.version>1.18.4</lombok.version>
		<mysql.version>8.0.18</mysql.version>
        <postgresql.version>42.2.5</postgresql.version>
		<#if databasePool == "Druid">
		<druid.version>1.1.10</druid.version>
		</#if>
		<#if databasePool == "HikariCP">
		<hikariCP.version>2.6.1</hikariCP.version>
		</#if>
		<#if ifUseSwagger == "是">
		<swagger2.version>2.9.2</swagger2.version>
        </#if>

		<!-- <fastjson.version>1.2.11</fastjson.version> <gson.version>2.3.1</gson.version> -->

		<!-- <commons-lang.version>2.6</commons-lang.version> <commons-io.version>2.5</commons-io.version>
			<jdom.version>2.0.2</jdom.version> <activeMQ.version>5.11.4</activeMQ.version>
			<javassist.version>3.12.1.GA</javassist.version> <transaction.version>1.1</transaction.version> -->
	</properties>


	<dependencies>
		<!-- 1. junit 依赖 begin junit3.0使用编程方式运行,junit4.0使用注解方式运行 -->
		<dependency>
			<groupId>junit</groupId>
			<artifactId>junit</artifactId>
			<version><#noparse>${</#noparse>junit.version}</version>
		</dependency>
		<!-- junit 依赖 end -->

		<!-- 2. Servlet web相关依赖 begin -->
		<dependency>
			<groupId>javax</groupId>
			<artifactId>javaee-api</artifactId>
			<version><#noparse>${</#noparse>javaee-api.version}</version>
		</dependency>
        <!-- https://mvnrepository.com/artifact/org.springframework.boot/spring-boot-starter-validation -->
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-validation</artifactId>
            <version>2.2.6.RELEASE</version>
        </dependency>

        <dependency>
			<groupId>jstl</groupId>
			<artifactId>jstl</artifactId>
			<version><#noparse>${</#noparse>jstl.version}</version>
		</dependency>
		<!-- Servlet web相关依赖 end -->

		<!-- 3. aspectjweaver 依赖 begin -->
		<dependency>
			<groupId>org.aspectj</groupId>
			<artifactId>aspectjweaver</artifactId>
			<version><#noparse>${</#noparse>aspectjweaver.version}</version>
		</dependency>
		<!-- aspectjweaver 依赖 end -->

		<!-- 4. spring依赖 begin -->
		<!-- spring核心依赖 -->
		<dependency>
			<groupId>org.springframework</groupId>
			<artifactId>spring-core</artifactId>
			<version><#noparse>${</#noparse>spring.version}</version>
		</dependency>

		<!-- spring ioc依赖 -->
		<dependency>
			<groupId>org.springframework</groupId>
			<artifactId>spring-beans</artifactId>
			<version><#noparse>${</#noparse>spring.version}</version>
		</dependency>

		<!-- spring aop依赖 -->
		<dependency>
			<groupId>org.springframework</groupId>
			<artifactId>spring-aop</artifactId>
			<version><#noparse>${</#noparse>spring.version}</version>
		</dependency>

		<!-- spring 扩展依赖 -->
		<dependency>
			<groupId>org.springframework</groupId>
			<artifactId>spring-context</artifactId>
			<version><#noparse>${</#noparse>spring.version}</version>
		</dependency>

		<!--spring dao层依赖 -->
		<dependency>
			<groupId>org.springframework</groupId>
			<artifactId>spring-jdbc</artifactId>
			<version><#noparse>${</#noparse>spring.version}</version>
		</dependency>
		<dependency>
			<groupId>org.springframework</groupId>
			<artifactId>spring-tx</artifactId>
			<version><#noparse>${</#noparse>spring.version}</version>
		</dependency>

		<!-- spring web相关依赖 -->
		<dependency>
			<groupId>org.springframework</groupId>
			<artifactId>spring-web</artifactId>
			<version><#noparse>${</#noparse>spring.version}</version>
		</dependency>
		<dependency>
			<groupId>org.springframework</groupId>
			<artifactId>spring-webmvc</artifactId>
			<version><#noparse>${</#noparse>spring.version}</version>
		</dependency>

		<!-- spring test相关依赖 -->
		<dependency>
			<groupId>org.springframework</groupId>
			<artifactId>spring-test</artifactId>
			<version><#noparse>${</#noparse>spring.version}</version>
		</dependency>
		<!-- spring依赖 end -->

		<!-- 5. mybatis依赖 begin -->
		<dependency>
			<groupId>org.mybatis</groupId>
			<artifactId>mybatis</artifactId>
			<version><#noparse>${</#noparse>mybatis.version}</version>
		</dependency>

		<dependency>
			<groupId>org.mybatis</groupId>
			<artifactId>mybatis-spring</artifactId>
			<version><#noparse>${</#noparse>mybatis-spring.version}</version>
		</dependency>
		<!-- mybatis依赖 end -->

    <#if isMutiDataSource>
        <!-- 数据库相关配置 根据需要自行选择-->
        <dependency>
            <groupId>mysql</groupId>
            <artifactId>mysql-connector-java</artifactId>
            <version><#noparse>${</#noparse>mysql.version}</version>
            <scope>runtime</scope>
        </dependency>
        <dependency>
            <groupId>org.postgresql</groupId>
            <artifactId>postgresql</artifactId>
            <version><#noparse>${</#noparse>postgresql.version}</version>
            <scope>runtime</scope>
        </dependency>
        <dependency>
            <groupId>com.ojdbc</groupId>
            <artifactId>ojdbc</artifactId>
            <version>1.0.0</version>
            <scope>system</scope>
            <systemPath><#noparse>${</#noparse>project.basedir<#noparse>}</#noparse>/src/main/webapp/WEB-INF/lib/ojdbc-11.2.0.3.0.jar</systemPath>
        </dependency>
        <dependency>
            <groupId>com.microsoft.sqlserver</groupId>
            <artifactId>sqljdbc6.0</artifactId>
            <version>6.0</version>
            <scope>system</scope>
            <systemPath><#noparse>${</#noparse>project.basedir<#noparse>}</#noparse>/src/main/webapp/WEB-INF/lib/sqljdbc42.jar</systemPath>
        </dependency>
    <#else>
        <!-- 6. 数据库相关依赖 begin -->
        <#if dataBaseType == "mysql">
            <dependency>
                <groupId>mysql</groupId>
                <artifactId>mysql-connector-java</artifactId>
                <version><#noparse>${</#noparse>mysql.version}</version>
                <scope>runtime</scope>
            </dependency>
        </#if>

        <#if dataBaseType == "postgresql">
            <!-- 数据库相关配置 -->
            <dependency>
                <groupId>org.postgresql</groupId>
                <artifactId>postgresql</artifactId>
                <version><#noparse>${</#noparse>postgresql.version}</version>
                <scope>runtime</scope>
            </dependency>
        </#if>

        <#if dataBaseType == "oracle">
            <dependency>
                <groupId>com.ojdbc</groupId>
                <artifactId>ojdbc</artifactId>
                <version>1.0.0</version>
                <scope>system</scope>
                <systemPath><#noparse>${</#noparse>project.basedir<#noparse>}</#noparse>/src/main/webapp/WEB-INF/lib/ojdbc-11.2.0.3.0.jar</systemPath>
            </dependency>
        </#if>

        <#if dataBaseType == "sqlserver">
            <dependency>
                <groupId>com.microsoft.sqlserver</groupId>
                <artifactId>sqljdbc6.0</artifactId>
                <version>6.0</version>
                <scope>system</scope>
                <systemPath><#noparse>${</#noparse>project.basedir<#noparse>}</#noparse>/src/main/webapp/WEB-INF/lib/sqljdbc42.jar</systemPath>
            </dependency>
        </#if>
    </#if>


		<#if databasePool == "Druid">
		<dependency>
			<groupId>com.alibaba</groupId>
			<artifactId>druid</artifactId>
			<version><#noparse>${</#noparse>druid.version}</version>
		</dependency>
		</#if>

		<#if databasePool == "HikariCP">
		<dependency>
            <groupId>com.zaxxer</groupId>
            <artifactId>HikariCP</artifactId>
            <version><#noparse>${</#noparse>hikariCP.version}</version>
		</dependency>
		</#if>

		<#if ifUseSwagger == "是">
		<!--springfox的核心jar包 -->
        <dependency>
            <groupId>io.springfox</groupId>
            <artifactId>springfox-swagger2</artifactId>
            <version><#noparse>${</#noparse>swagger2.version}</version>
        </dependency>
        <!--swagger-ui的jar包(里面包含了swagger的界面静态文件) -->
        <dependency>
            <groupId>io.springfox</groupId>
            <artifactId>springfox-swagger-ui</artifactId>
            <version><#noparse>${</#noparse>swagger2.version}</version>
        </dependency>
        <dependency>
            <groupId>com.github.xiaoymin</groupId>
            <artifactId>swagger-bootstrap-ui</artifactId>
            <version>1.9.5</version>
        </dependency>
		</#if>

		<!-- 数据库相关依赖 end -->

		<!-- 7. log日志依赖 begin -->
		<!-- Logback -->
		<!-- https://mvnrepository.com/artifact/ch.qos.logback/logback-classic -->
		<dependency>
			<groupId>ch.qos.logback</groupId>
			<artifactId>logback-classic</artifactId>
			<version>1.2.3</version>
		</dependency>
		<!-- https://mvnrepository.com/artifact/ch.qos.logback/logback-core -->
		<dependency>
			<groupId>ch.qos.logback</groupId>
			<artifactId>logback-core</artifactId>
			<version>1.2.3</version>
		</dependency>
		<!-- https://mvnrepository.com/artifact/org.logback-extensions/logback-ext-spring -->
		<dependency>
			<groupId>org.logback-extensions</groupId>
			<artifactId>logback-ext-spring</artifactId>
			<version>0.1.4</version>
		</dependency>
		<!-- https://mvnrepository.com/artifact/org.slf4j/slf4j-api -->
		<dependency>
			<groupId>org.slf4j</groupId>
			<artifactId>slf4j-api</artifactId>
			<version>1.7.25</version>
		</dependency>
		<!-- https://mvnrepository.com/artifact/org.slf4j/jcl-over-slf4j -->
		<dependency>
			<groupId>org.slf4j</groupId>
			<artifactId>jcl-over-slf4j</artifactId>
			<version>1.7.25</version>
		</dependency>
		<!--log日志依赖 end -->

		<!-- fileupload 文件上传 -->
		<dependency>
			<groupId>commons-fileupload</groupId>
			<artifactId>commons-fileupload</artifactId>
			<version><#noparse>${</#noparse>fileupload.version}</version>
		</dependency>

		<!-- POI 相关依赖 -->
        <dependency>
            <groupId>org.apache.poi</groupId>
            <artifactId>poi</artifactId>
            <version>4.0.1</version>
        </dependency>
        <dependency>
            <groupId>org.apache.poi</groupId>
            <artifactId>poi-ooxml</artifactId>
            <version>4.0.1</version>
        </dependency>

		<!-- 二维码依赖 -->
		<dependency>
			<groupId>com.google.zxing</groupId>
			<artifactId>core</artifactId>
			<version>3.3.0</version>
		</dependency>

		<!-- https://mvnrepository.com/artifact/com.fasterxml.jackson.core/jackson-databind -->
		<dependency>
			<groupId>com.fasterxml.jackson.core</groupId>
			<artifactId>jackson-databind</artifactId>
			<version>2.9.8</version>
		</dependency>

		<!-- https://mvnrepository.com/artifact/com.fasterxml.jackson.core/jackson-core -->
		<dependency>
			<groupId>com.fasterxml.jackson.core</groupId>
			<artifactId>jackson-core</artifactId>
			<version>2.9.8</version>
		</dependency>

		<!-- 引入thymeleaf -->
		<dependency>
			<groupId>org.thymeleaf</groupId>
			<artifactId>thymeleaf</artifactId>
			<version>3.0.0.RELEASE</version>
		</dependency>

		<dependency>
			<groupId>org.thymeleaf</groupId>
			<artifactId>thymeleaf-spring4</artifactId>
			<version>3.0.0.RELEASE</version>
			<exclusions>
				<exclusion>
					<artifactId>javassist</artifactId>
					<groupId>org.javassist</groupId>
				</exclusion>
			</exclusions>
		</dependency>

		<dependency>
			<groupId>org.thymeleaf.extras</groupId>
			<artifactId>thymeleaf-extras-springsecurity4</artifactId>
			<version>3.0.0.RELEASE</version>
		</dependency>

		<dependency>
			<groupId>org.thymeleaf.extras</groupId>
			<artifactId>thymeleaf-extras-java8time</artifactId>
			<version>3.0.0.RELEASE</version>
		</dependency>

		<!-- https://mvnrepository.com/artifact/com.fasterxml.jackson.core/jackson-annotations -->
		<dependency>
			<groupId>com.fasterxml.jackson.core</groupId>
			<artifactId>jackson-annotations</artifactId>
			<version>2.9.8</version>
		</dependency>

        <!-- lombok-->
        <dependency>
            <groupId>org.projectlombok</groupId>
            <artifactId>lombok</artifactId>
            <version><#noparse>${</#noparse>lombok.version}</version>
        </dependency>

        <!--apache-->
        <dependency>
            <groupId>org.apache.commons</groupId>
            <artifactId>commons-lang3</artifactId>
            <version>3.8.1</version>
        </dependency>

	</dependencies>

	<build>
		<finalName>${projectName}</finalName>
		<resources>
			<resource>
				<directory>src/main/java</directory>
				<includes>
					<include>**/*.xml</include>
				</includes>
				<filtering>false</filtering>
			</resource>
			<resource>
				<directory>src/main/resources</directory>
				<includes>
					<include>*.xml</include>
					<include>*.properties</include>
				</includes>
				<filtering>false</filtering>
			</resource>
		</resources>
		<plugins>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-compiler-plugin</artifactId>
				<version>3.0</version>
				<configuration>
					<source>1.8</source>
					<target>1.8</target>
					<encoding>UTF-8</encoding>
            <#if isMutiDataSource>
                    <compilerArguments>
                        <extdirs>src/main/webapp/WEB-INF/lib/ojdbc-11.2.0.3.0.jar</extdirs>
                        <extdirs>src/main/webapp/WEB-INF/lib/sqljdbc42.jar</extdirs>
                    </compilerArguments>
            <#else>
                <#if dataBaseType == "oracle">
                    <compilerArguments>
                        <extdirs>src/main/webapp/WEB-INF/lib/ojdbc-11.2.0.3.0.jar</extdirs>
                    </compilerArguments>
                </#if>
                <#if dataBaseType == "sqlserver">
                    <compilerArguments>
                        <extdirs>src/main/webapp/WEB-INF/lib/sqljdbc42.jar</extdirs>
                    </compilerArguments>
                </#if>
            </#if>
				</configuration>
			</plugin>
		</plugins>
	</build>
	</#if>

</project>
