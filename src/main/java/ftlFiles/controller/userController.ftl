package <#if isCloudModel>${cloudSysEngName}<#else>${packageName}</#if>.controller;
<#if isAuthority>
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import <#if isCloudModel>${cloudSysEngName}<#else>${packageName}</#if>.entity.CmSysUserEntity;
import <#if isCloudModel>${cloudSysEngName}<#else>${packageName}</#if>.service.CmSysUserService;
import <#if isCloudModel>${cloudSysEngName}<#else>${packageName}</#if>.utils.SessionUtil;

import javax.servlet.http.HttpServletRequest;

/**
 * @author zrx
 */
@RestController
@Api(tags = "login接口")
@RequestMapping("/login")
public class LoginController {


	private final CmSysUserService service;

	@Autowired
	public LoginController(CmSysUserService service) {
		this.service = service;
	}

	@ApiOperation(value = "登录")
	@PostMapping(value = "/doLogin")
	public CmSysUserEntity doLogin(@RequestBody CmSysUserEntity user, HttpServletRequest request) {
		return service.doLogin(user, request);
	}

	@ApiOperation(value = "退出登录")
	@PostMapping(value = "/doLogOut")
	public void doLogOut(HttpServletRequest request) {
		SessionUtil.logOut(request);
	}
}
<#else>
import <#if isCloudModel>${cloudSysEngName}<#else>${packageName}</#if>.core.exception.BusinessException;
<#if loginModel == "动态用户">
import org.springframework.beans.factory.annotation.Autowired;
</#if>
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import <#if isCloudModel>${cloudSysEngName}<#else>${packageName}</#if>.entity.User;
import <#if isCloudModel>${cloudSysEngName}<#else>${packageName}</#if>.utils.SessionUtil;
<#if loginModel == "动态用户">
import <#if isCloudModel>${cloudSysEngName}<#else>${packageName}</#if>.service.LoginService;
</#if>
<#if ifUseSwagger == "是">
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
</#if>
import javax.servlet.http.HttpServletRequest;

@RestController
<#if ifUseSwagger == "是">
@Api(tags = "login接口")
</#if>
@RequestMapping("/login")
public class LoginController {
<#if loginModel == "动态用户">
	private final LoginService service;
	@Autowired
	public LoginController(LoginService service) {
		this.service = service;
	}

	<#if ifUseSwagger == "是">
	@ApiOperation(value = "登录")
	</#if>
	@PostMapping(value = "/doLogin")
	public User doLogin(@RequestBody User user, HttpServletRequest request) {
		User currentUser = service.login(user);
		if (currentUser != null) {
        <#if isRedisSingleLogin>
            SessionUtil.onLogin(currentUser);
        <#else>
            SessionUtil.onLogin(currentUser, request);
        </#if>
		} else {
			throw new BusinessException("用户名或密码错误");
		}
        return currentUser;
	}
</#if>
<#if loginModel == "静态用户">
	<#if ifUseSwagger == "是">
	@ApiOperation(value = "登录")
	</#if>
	@PostMapping(value = "/doLogin")
	public User doLogin(@RequestBody User user, HttpServletRequest request) {
		if (${userCondition}) {
        <#if isRedisSingleLogin>
            SessionUtil.onLogin(user);
        <#else>
            SessionUtil.onLogin(user, request);
        </#if>
		} else {
			throw new BusinessException("用户名或密码错误");
		}
        return user;
	}
</#if>
	<#if ifUseSwagger == "是">
	@ApiOperation(value = "退出登录")
	</#if>
	@PostMapping(value = "/doLogOut")
	public void doLogOut(HttpServletRequest request) {
        SessionUtil.logOut(request);
	}
}
</#if>
