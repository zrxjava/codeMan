<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE mapper PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN" "http://mybatis.org/dtd/mybatis-3-mapper.dtd" >
<mapper namespace="<#if isCloudModel>${cloudSysEngName}<#else>${packageName}</#if>.dao.${capCurrentMutiEng}MutiDao">

	<#list currentMethodMap?keys as key>

        <#assign formSql = currentMethodMap["${key}"].formSql/>
		<#assign tableFiledModels = currentMethodMap["${key}"].tableFiledModels/>
		<#assign tableConditionModels = currentMethodMap["${key}"].tableConditionModels/>
		<#assign entityName = currentMethodMap["${key}"].entityName/>

		<!--分页（模糊查询的公共条件）-->
		<sql id="${key}Conditions">
			<#list tableConditionModels as data>
                <#if data.compareText==">= && <=">
                    <if test="start${data.anotherTableName}_${data.filed_eng} != null and start${data.anotherTableName}_${data.filed_eng} != '' ">
                        ${data.relation} ${data.anotherTableName}.${data.filed_eng} <![CDATA[>=]]> <#noparse>#{</#noparse>start${data.anotherTableName}_${data.filed_eng}<#noparse>}</#noparse>
                    </if>
                    <if test="end${data.anotherTableName}_${data.filed_eng} != null and end${data.anotherTableName}_${data.filed_eng} != '' ">
                        and ${data.anotherTableName}.${data.filed_eng} <![CDATA[<=]]> <#noparse>#{</#noparse>end${data.anotherTableName}_${data.filed_eng}<#noparse>}</#noparse>
                    </if>
                <#else>
                    <#if data.unchangeValue!="">
                        ${data.relation} ${data.anotherTableName}.${data.filed_eng} <![CDATA[${data.compareText}]]> '${data.unchangeValue}'
                    <#else>
                    <if test="${data.anotherTableName}_${data.filed_eng} != null and ${data.anotherTableName}_${data.filed_eng} != '' ">
                    <#if data.compareText=="like">
                        <#if dataBaseType == "mysql">
                        ${data.relation} ${data.anotherTableName}.${data.filed_eng} like <#noparse>"%"#{</#noparse>${data.anotherTableName}_${data.filed_eng}<#noparse>}"%"</#noparse>
                        </#if>
                        <#if dataBaseType == "oracle" || dataBaseType == "postgresql">
                        ${data.relation} ${data.anotherTableName}.${data.filed_eng} like <#noparse>'%' || #{</#noparse>${data.anotherTableName}_${data.filed_eng}<#noparse>} || '%'</#noparse>
                        </#if>
                        <#if dataBaseType == "sqlserver">
                        ${data.relation} ${data.anotherTableName}.${data.filed_eng} like <#noparse>'%' + #{</#noparse>${data.anotherTableName}_${data.filed_eng}<#noparse>} + '%'</#noparse>
                        </#if>
                    <#else>
                        ${data.relation} ${data.anotherTableName}.${data.filed_eng} <![CDATA[${data.compareText}]]> <#noparse>#{</#noparse>${data.anotherTableName}_${data.filed_eng}<#noparse>}</#noparse>
                    </#if>
                    </if>
                    </#if>
                </#if>
            </#list>
		</sql>

		<select id="${key}" parameterType="<#if isCloudModel>${cloudSysEngName}<#else>${packageName}</#if>.entity.${entityName?cap_first}Muti" resultType="<#if isCloudModel>${cloudSysEngName}<#else>${packageName}</#if>.entity.${entityName?cap_first}Muti">

		<#if dataBaseType == "mysql" || dataBaseType == "postgresql" || dataBaseType == "sqlserver" >
			select
			<#list tableFiledModels as data>
				${data.anotherTableName}.${data.filedText_eng} ${data.anotherFiledName}<#if data_has_next>,</#if>
			</#list>
			${formSql}
            <include refid="${key}Conditions"/>
        <#if dataBaseType == "sqlserver">
            <if test="orderStr == '' or orderStr == null">
                order by ${tableFiledModels[0].filedText_eng}
            </if>
        </#if>
			<if test="orderStr != '' and orderStr != null">
				order by <#noparse>${</#noparse>orderStr<#noparse>}</#noparse>
			</if>
            <#if dataBaseType == "mysql">
            <if test="start != null and pageSize != null">
                limit <#noparse>#{</#noparse>start<#noparse>}</#noparse>,<#noparse>#{</#noparse>pageSize<#noparse>}</#noparse>
            </if>
            </#if>
            <#if dataBaseType == "postgresql">
            <if test="start != null and pageSize != null">
                limit <#noparse>#{</#noparse>pageSize<#noparse>}</#noparse> offset <#noparse>#{</#noparse>start<#noparse>}</#noparse>
            </if>
            </#if>
            <#if dataBaseType == "sqlserver">
            <if test="start != null and pageSize != null">
                offset <#noparse>#{</#noparse>start<#noparse>}</#noparse> rows fetch next <#noparse>#{</#noparse>pageSize<#noparse>}</#noparse> rows only
            </if>
            </#if>
		</#if>

		<#if dataBaseType == "oracle">
			SELECT
			<#list tableFiledModels as data>
				${data.anotherTableName}.${data.filedText_eng} ${data.anotherFiledName}<#if data_has_next>,</#if>
			</#list>

			<if test="startIndex == null or endIndex == null">
			${formSql}
                  <include refid="${key}Conditions"/>
                  <if test="orderStr != '' and orderStr != null">
                    order by <#noparse>${</#noparse>orderStr<#noparse>}</#noparse>
                  </if>
			</if>
			<if test="startIndex != null and endIndex != null">

	  			FROM (SELECT tb2.*, ROWNUM rowno

	          	FROM (SELECT tb1.*
	                  FROM (SELECT
	                 <#list tableFiledModels as data>
					  ${data.anotherTableName}.${data.filedText_eng} ${data.anotherFiledName}<#if data_has_next>,</#if>
					 </#list>
	                  ${formSql}) tb1
                      <include refid="${key}Conditions"/>
	                  <if test="orderStr != '' and orderStr != null">
						order by <#noparse>${</#noparse>orderStr<#noparse>}</#noparse>
					  </if>
	                  ) tb2

	           <![CDATA[WHERE ROWNUM <= <#noparse>#{</#noparse>endIndex<#noparse>}</#noparse>) tb3

	 		   WHERE tb3.rowno > <#noparse>#{</#noparse>startIndex<#noparse>}</#noparse>]]>
	 		</if>
		</#if>

	</select>

	<select id="${key}Count" parameterType="<#if isCloudModel>${cloudSysEngName}<#else>${packageName}</#if>.entity.${entityName?cap_first}Muti" resultType="java.lang.Long">
		SELECT
		count(1)
		${formSql}
       	<include refid="${key}Conditions"/>
	</select>

    </#list>
</mapper>
