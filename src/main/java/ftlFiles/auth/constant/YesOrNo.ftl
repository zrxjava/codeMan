package <#if isCloudModel>${cloudSysEngName}<#else>${packageName}</#if>.constant;


/**
 * 是或否状态枚举
 *
 * @author wanglidong
 */
public enum YesOrNo {

	/**
	 * 否
	 */
	NO(0, "否"),

	/**
	 * 是
	 */
	YES(1, "是");

	private Integer code;
	private String value;

	YesOrNo(Integer code, String value) {
		this.code = code;
		this.value = value;
	}

	public String getValue() {
		return this.value;
	}

	public Integer getCode() {
		return this.code;
	}

	/**
	 * 根据code获取对应的枚举
	 *
	 * @param code
	 * @return
	 */
	public static YesOrNo getByCode(Integer code) {
		for (YesOrNo obj : YesOrNo.values()) {
			if (obj.code.equals(code)) {
				return obj;
			}
		}

		return null;
	}
}
