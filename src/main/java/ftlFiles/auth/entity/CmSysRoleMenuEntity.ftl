package <#if isCloudModel>${cloudSysEngName}<#else>${packageName}</#if>.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

@ApiModel
@Data
@EqualsAndHashCode(callSuper = false)
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class CmSysRoleMenuEntity extends CommonEntity implements Serializable {

	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * 主键id
	 */
	@ApiModelProperty(value = "主键id")
	private Long roleMenuId;
	/**
	 * 角色id
	 */
	@ApiModelProperty(value = "角色id")
	private Long roleId;

	/**
	 * 菜单id
	 */
	@ApiModelProperty(value = "菜单id")
	private Long menuId;
	/**
	 * create_time
	 */
	@ApiModelProperty(value = "create_time", name = "createTime")
	private Date createTime;
	/**
	 * update_time
	 */
	@ApiModelProperty(value = "update_time", name = "updateTime")
	private Date updateTime;
	/**
	 * create_user_id
	 */
	@ApiModelProperty(value = "create_user_id", name = "createUserId")
	private Long createUserId;
	/**
	 * update_user_id
	 */
	@ApiModelProperty(value = "update_user_id", name = "updateUserId")
	private Long updateUserId;

}
