package <#if isCloudModel>${cloudSysEngName}<#else>${packageName}</#if>.dao;

<#if frameWorkVal=="springBoot">
import org.apache.ibatis.annotations.Mapper;
</#if>
import org.springframework.stereotype.Repository;
<#if entityName??>
import <#if isCloudModel>${cloudSysEngName}<#else>${packageName}</#if>.entity.${entityName};
<#else>
import java.util.Map;
</#if>



<#if frameWorkVal=="springBoot">
@Mapper
</#if>
@Repository
<#if !entityName??>
public interface ${IDaoName} extends BaseDao<Map<String, Object>> {

}
<#else>
public interface ${IDaoName} extends BaseDao<${entityName}> {

}
</#if>
