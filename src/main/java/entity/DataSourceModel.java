package entity;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * @ClassName DataSourceModel
 * @Author zrx
 * @Date 2021/11/30 23:03
 */
public class DataSourceModel implements Serializable {

	private static final long serialVersionUID = 3082533432934169490L;

	/**
	 * 自定义的数据源名称
	 */
	private String dataSourceName;

	private String dataBaseTypeVal = "mysql";

	private String dataBaseIpVal;

	private String dataBasePortVal;

	private String dataBasePwdVal;

	private String dataBaseNameVal;

	private String dataBaseUserNameVal;

	private String dataBaseUrl;

	private String dataBaseDriverClass;

	private String tableName;

	// 最终设置的表的主键
	private Map<String, List<String>> primaryKeyListMap = new HashMap<>();

	// 最终设置的展示的字段的相关信息
	private Map<String, List<DatabaseModel>> columnMsgMap = new HashMap<>();
	// 最终设置的更新的字段的相关信息
	private Map<String, List<DatabaseModel>> updateColumnMsgMap = new HashMap<>();
	// 最终设置的查询的字段的相关信息
	private Map<String, List<DatabaseModel>> queryColumnMsgMap = new HashMap<>();

	// 最终设置的所有字段的相关信息
	private Map<String, List<DatabaseModel>> allColumnMsgMap = new HashMap<>();

	// 最终设置的表的中文名称（key:英文 value:中文）
	private Map<String, String> currentTableCnNameMap = new HashMap<>();

	// 最终设置的表的参数类型（传参是JavaBean还是Map）
	private Map<String, String> tableParamConfig = new HashMap<>();

	// 最终设置的表的字段类项和名称
	private Map<String, List<TableNameAndType>> currentTableNameAndTypes = new HashMap<>();

	// private Map<String, TablesQueryModel> tablesQueryMap = new HashMap<>();

	// 多表查询的配置信息 key:模块名 value: key：方法名 value：对应的sql
	private Map<String, Map<String, TablesQueryModel>> tablesQueryMap = new HashMap<>();

	// 模块名和模块中文名
	private Map<String, String> tablesQueryEndAndCnMap = new HashMap<>();

	public String getDataSourceName() {
		return dataSourceName;
	}

	public void setDataSourceName(String dataSourceName) {
		this.dataSourceName = dataSourceName;
	}

	public String getDataBaseTypeVal() {
		return dataBaseTypeVal;
	}

	public void setDataBaseTypeVal(String dataBaseTypeVal) {
		this.dataBaseTypeVal = dataBaseTypeVal;
	}

	public String getDataBaseIpVal() {
		return dataBaseIpVal;
	}

	public void setDataBaseIpVal(String dataBaseIpVal) {
		this.dataBaseIpVal = dataBaseIpVal;
	}

	public String getDataBasePortVal() {
		return dataBasePortVal;
	}

	public void setDataBasePortVal(String dataBasePortVal) {
		this.dataBasePortVal = dataBasePortVal;
	}

	public String getDataBasePwdVal() {
		return dataBasePwdVal;
	}

	public void setDataBasePwdVal(String dataBasePwdVal) {
		this.dataBasePwdVal = dataBasePwdVal;
	}

	public String getDataBaseNameVal() {
		return dataBaseNameVal;
	}

	public void setDataBaseNameVal(String dataBaseNameVal) {
		this.dataBaseNameVal = dataBaseNameVal;
	}

	public String getDataBaseUserNameVal() {
		return dataBaseUserNameVal;
	}

	public void setDataBaseUserNameVal(String dataBaseUserNameVal) {
		this.dataBaseUserNameVal = dataBaseUserNameVal;
	}

	public String getDataBaseUrl() {
		return dataBaseUrl;
	}

	public void setDataBaseUrl(String dataBaseUrl) {
		this.dataBaseUrl = dataBaseUrl;
	}

	public String getDataBaseDriverClass() {
		return dataBaseDriverClass;
	}

	public void setDataBaseDriverClass(String dataBaseDriverClass) {
		this.dataBaseDriverClass = dataBaseDriverClass;
	}

	public String getTableName() {
		return tableName;
	}

	public void setTableName(String tableName) {
		this.tableName = tableName;
	}

	public Map<String, List<String>> getPrimaryKeyListMap() {
		return primaryKeyListMap;
	}

	public void setPrimaryKeyListMap(Map<String, List<String>> primaryKeyListMap) {
		this.primaryKeyListMap = primaryKeyListMap;
	}

	public Map<String, List<DatabaseModel>> getColumnMsgMap() {
		return columnMsgMap;
	}

	public void setColumnMsgMap(Map<String, List<DatabaseModel>> columnMsgMap) {
		this.columnMsgMap = columnMsgMap;
	}

	public Map<String, List<DatabaseModel>> getUpdateColumnMsgMap() {
		return updateColumnMsgMap;
	}

	public void setUpdateColumnMsgMap(Map<String, List<DatabaseModel>> updateColumnMsgMap) {
		this.updateColumnMsgMap = updateColumnMsgMap;
	}

	public Map<String, List<DatabaseModel>> getQueryColumnMsgMap() {
		return queryColumnMsgMap;
	}

	public void setQueryColumnMsgMap(Map<String, List<DatabaseModel>> queryColumnMsgMap) {
		this.queryColumnMsgMap = queryColumnMsgMap;
	}

	public Map<String, List<DatabaseModel>> getAllColumnMsgMap() {
		return allColumnMsgMap;
	}

	public void setAllColumnMsgMap(Map<String, List<DatabaseModel>> allColumnMsgMap) {
		this.allColumnMsgMap = allColumnMsgMap;
	}

	public Map<String, String> getCurrentTableCnNameMap() {
		return currentTableCnNameMap;
	}

	public void setCurrentTableCnNameMap(Map<String, String> currentTableCnNameMap) {
		this.currentTableCnNameMap = currentTableCnNameMap;
	}

	public Map<String, String> getTableParamConfig() {
		return tableParamConfig;
	}

	public void setTableParamConfig(Map<String, String> tableParamConfig) {
		this.tableParamConfig = tableParamConfig;
	}

	public Map<String, List<TableNameAndType>> getCurrentTableNameAndTypes() {
		return currentTableNameAndTypes;
	}

	public void setCurrentTableNameAndTypes(Map<String, List<TableNameAndType>> currentTableNameAndTypes) {
		this.currentTableNameAndTypes = currentTableNameAndTypes;
	}

	public Map<String, Map<String, TablesQueryModel>> getTablesQueryMap() {
		return tablesQueryMap;
	}

	public void setTablesQueryMap(Map<String, Map<String, TablesQueryModel>> tablesQueryMap) {
		this.tablesQueryMap = tablesQueryMap;
	}

	public Map<String, String> getTablesQueryEndAndCnMap() {
		return tablesQueryEndAndCnMap;
	}

	public void setTablesQueryEndAndCnMap(Map<String, String> tablesQueryEndAndCnMap) {
		this.tablesQueryEndAndCnMap = tablesQueryEndAndCnMap;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		DataSourceModel that = (DataSourceModel) o;
		return Objects.equals(dataSourceName, that.dataSourceName);
	}

	@Override
	public int hashCode() {
		return Objects.hash(dataSourceName);
	}

	@Override
	public String toString() {
		return "DataSourceModel{" +
				"dataSourceName='" + dataSourceName + '\'' +
				", dataBaseTypeVal='" + dataBaseTypeVal + '\'' +
				", dataBaseIpVal='" + dataBaseIpVal + '\'' +
				", dataBasePortVal='" + dataBasePortVal + '\'' +
				", dataBasePwdVal='" + dataBasePwdVal + '\'' +
				", dataBaseNameVal='" + dataBaseNameVal + '\'' +
				", dataBaseUserNameVal='" + dataBaseUserNameVal + '\'' +
				", dataBaseUrl='" + dataBaseUrl + '\'' +
				", dataBaseDriverClass='" + dataBaseDriverClass + '\'' +
				", tableName='" + tableName + '\'' +
				", primaryKeyListMap=" + primaryKeyListMap +
				", columnMsgMap=" + columnMsgMap +
				", updateColumnMsgMap=" + updateColumnMsgMap +
				", queryColumnMsgMap=" + queryColumnMsgMap +
				", allColumnMsgMap=" + allColumnMsgMap +
				", currentTableCnNameMap=" + currentTableCnNameMap +
				", tableParamConfig=" + tableParamConfig +
				", currentTableNameAndTypes=" + currentTableNameAndTypes +
				", tablesQueryMap=" + tablesQueryMap +
				", tablesQueryEndAndCnMap=" + tablesQueryEndAndCnMap +
				'}';
	}
}
