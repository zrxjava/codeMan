package entity;

import constant.ServiceTypeComboBox;

import java.io.Serializable;

public class MakeEntityModel implements Serializable {

	private static final long serialVersionUID = 6064009465404480255L;

	private String filedName_eng;

	private String filedName_cn;

	private ServiceTypeComboBox<String> servieceTypeCombox;

	private String serviceType;

	public String getFiledName_eng() {
		return filedName_eng;
	}

	public void setFiledName_eng(String filedName_eng) {
		this.filedName_eng = filedName_eng;
	}

	public String getFiledName_cn() {
		return filedName_cn;
	}

	public void setFiledName_cn(String filedName_cn) {
		this.filedName_cn = filedName_cn;
	}

	public ServiceTypeComboBox<String> getServieceTypeCombox() {
		return servieceTypeCombox;
	}

	public void setServieceTypeCombox(ServiceTypeComboBox<String> servieceTypeCombox) {
		this.servieceTypeCombox = servieceTypeCombox;
	}

	public String getServiceType() {
		return serviceType;
	}

	public void setServiceType(String serviceType) {
		this.serviceType = serviceType;
	}

}
