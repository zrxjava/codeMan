package entity;

import java.io.Serializable;

public class EntityFieldModel implements Serializable {

	private static final long serialVersionUID = -3497694245968980389L;

	//字段名称
    private String fieldName;

    //字段中文名称
    private String fieldName_cn;

    //类型
    private String fieldType;

    //比较关系
    private String compareText;

    public String getFieldName() {
        return fieldName;
    }

    public void setFieldName(String fieldName) {
        this.fieldName = fieldName;
    }

    public String getFieldName_cn() {
        return fieldName_cn;
    }

    public void setFieldName_cn(String fieldName_cn) {
        this.fieldName_cn = fieldName_cn;
    }

    public String getFieldType() {
        return fieldType;
    }

    public void setFieldType(String fieldType) {
        this.fieldType = fieldType;
    }

    public String getCompareText() {
        return compareText;
    }

    public void setCompareText(String compareText) {
        this.compareText = compareText;
    }

    @Override
    public boolean equals(Object obj) {

        EntityFieldModel entityFieldModel = (EntityFieldModel) obj;

        if (this.getFieldName().equals(entityFieldModel.getFieldName()) && !">= && <=".equals(entityFieldModel.getCompareText())) {
            return true;
        }
        return false;
    }

    @Override
    public int hashCode() {
        return this.getFieldName().hashCode() + this.getCompareText().hashCode();
    }
}
