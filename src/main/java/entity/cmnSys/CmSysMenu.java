package entity.cmnSys;

import constant.MenuLevel;

import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

/**
 * @ClassName CmSysMenu
 * @Author zrx
 * @Date 2021/5/18 10:58
 */
public class CmSysMenu implements Serializable {
	private static final long serialVersionUID = -1694183037876821982L;
	private Long menuId;
	private Long parentId;
	private Long orderNo;
	private String menuName;
	private String menuIcon;
	private String urlAddress;
	private Integer canDel;
	private Date createTime;
	private Date updateTime;
	private Long createUserId;
	private Long updateUserId;
	private String tableEng;
	private MenuLevel level;

	public Long getMenuId() {
		return menuId;
	}

	public void setMenuId(Long menuId) {
		this.menuId = menuId;
	}

	public Long getParentId() {
		return parentId;
	}

	public void setParentId(Long parentId) {
		this.parentId = parentId;
	}

	public Long getOrderNo() {
		return orderNo;
	}

	public void setOrderNo(Long orderNo) {
		this.orderNo = orderNo;
	}

	public String getMenuName() {
		return menuName;
	}

	public void setMenuName(String menuName) {
		this.menuName = menuName;
	}

	public String getMenuIcon() {
		return menuIcon;
	}

	public void setMenuIcon(String menuIcon) {
		this.menuIcon = menuIcon;
	}

	public String getUrlAddress() {
		return urlAddress;
	}

	public void setUrlAddress(String urlAddress) {
		this.urlAddress = urlAddress;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	public Long getCreateUserId() {
		return createUserId;
	}

	public void setCreateUserId(Long createUserId) {
		this.createUserId = createUserId;
	}

	public Long getUpdateUserId() {
		return updateUserId;
	}

	public void setUpdateUserId(Long updateUserId) {
		this.updateUserId = updateUserId;
	}


	public Integer getCanDel() {
		return canDel;
	}

	public void setCanDel(Integer canDel) {
		this.canDel = canDel;
	}

	public String getTableEng() {
		return tableEng;
	}

	public void setTableEng(String tableEng) {
		this.tableEng = tableEng;
	}

	public MenuLevel getLevel() {
		return level;
	}

	public void setLevel(MenuLevel level) {
		this.level = level;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		CmSysMenu cmSysMenu = (CmSysMenu) o;
		return Objects.equals(menuName, cmSysMenu.menuName) &&
				Objects.equals(urlAddress, cmSysMenu.urlAddress) &&
				Objects.equals(tableEng, cmSysMenu.tableEng) &&
				level == cmSysMenu.level;
	}

	@Override
	public int hashCode() {
		return Objects.hash(menuName, urlAddress, tableEng, level);
	}
}
