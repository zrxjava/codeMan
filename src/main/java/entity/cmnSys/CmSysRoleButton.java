package entity.cmnSys;

import java.util.Date;

/**
 * @ClassName CmSysButtonRoleMenu
 * @Author zrx
 * @Date 2021/5/18 10:58
 */
public class CmSysRoleButton {
	private Long roleButtonId;
	private Long buttonId;
	private Long roleId;
	private Date createTime;
	private Date updateTime;
	private Long createUserId;
	private Long updateUserId;

	public Long getRoleButtonId() {
		return roleButtonId;
	}

	public void setRoleButtonId(Long roleButtonId) {
		this.roleButtonId = roleButtonId;
	}

	public Long getButtonId() {
		return buttonId;
	}

	public void setButtonId(Long buttonId) {
		this.buttonId = buttonId;
	}

	public Long getRoleId() {
		return roleId;
	}

	public void setRoleId(Long roleId) {
		this.roleId = roleId;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	public Long getCreateUserId() {
		return createUserId;
	}

	public void setCreateUserId(Long createUserId) {
		this.createUserId = createUserId;
	}

	public Long getUpdateUserId() {
		return updateUserId;
	}

	public void setUpdateUserId(Long updateUserId) {
		this.updateUserId = updateUserId;
	}
}
